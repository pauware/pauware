PauWare v2 is an extension of PauWare 1.x which is available on https://pauware.univ-pau.fr/tech.html 

PauWare v2 is backward compatible with PauWare 1.x which means that a program written using PauWare 1.x can run with PauWare 2.x. The execution of a state machine is made with the same semantics and engine as PauWare 1.x

The extensions of PauWare v2 consist in mainly two parts: the capability to observe the execution of a state machine (the changes of active states, the calls of business operations...) for verification purposes and a refactoring of some parts of the API for an easier implementation of state machines.

PauWare v2 has been developped during the european projet MegaM@RT2: https://megamart2-ecsel.eu/