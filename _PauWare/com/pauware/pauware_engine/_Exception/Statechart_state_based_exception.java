/**
 * PauWare engine software (http://www.PauWare.com)
 * InterDeposit Digital Number IDDN.FR.001.360023.000.S.P.2006.000.10000
 * . Use of this software is subject to the restrictions of the LGPL license version 3 http://www.gnu.org/licenses/lgpl-3.0.en.html
 * . Use of PauWare is more precisely ruled by the Creative Commons Legal Code defining the "Attribution-NonCommercial-ShareAlike 2.5" license: http://www.gnu.org/licenses/lgpl-3.0.en.htmlby-nc-sa/2.5/legalcode
 */
package com.pauware.pauware_engine._Exception;

import com.pauware.pauware_engine._Core.AbstractStatechart;

/**
 * This class is concerned with abnormal state configuration/structuring exceptions.
 * It is typically thrown when states are linked together with <CODE>null</CODE> values.
 * It is also raised when states are not properly linked together with respect to the <I>nesting</I>, <I>xor</I> and <I>and</I> operators.
 * <p>
 * Compatibility: <I>PauWare Java EE</I> (Java SE/J2EE) and <I>PauWare Java ME</I> (Java SE/Java ME).
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public class Statechart_state_based_exception extends Statechart_exception {

    /**
     * This field refers to the first state involved in the problem creating the exception.
     */
    AbstractStatechart _first;
    /**
     * This field refers to the second state involved in the problem creating the exception.
     */
    AbstractStatechart _second;

    /**
     * @param message A text explaining the problem's cause.
     * @param first The first state involved in the problem.
     * @param second The second state involved in the problem.
     */
    public Statechart_state_based_exception(String message, AbstractStatechart first, AbstractStatechart second) {
        super(message);
        _first = first;
        _second = second;
    }

    /**
     * @return A message explaning why two states cause a problem in the configuration/layout of a state machine.
     */
    public String getMessage() {
        if (_first == null) {
            if (_second == null) {
                return super.getMessage() + ": 'first' and 'second' are both 'null'";
            } else {
                return super.getMessage() + ": 'first' is 'null' and 'second' is " + _second.name();
            }
        }
        if (_second == null) {
            return super.getMessage() + ": 'first' is " + _first.name() + " and 'second' is 'null'";
        }
        return super.getMessage() + ": 'first' is " + _first.name() + " and 'second' is " + _second.name();
    }
}
