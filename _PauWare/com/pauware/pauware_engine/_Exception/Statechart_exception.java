/**
 * PauWare engine software (http://www.PauWare.com)
 * InterDeposit Digital Number IDDN.FR.001.360023.000.S.P.2006.000.10000
 * . Use of this software is subject to the restrictions of the LGPL license version 3 http://www.gnu.org/licenses/lgpl-3.0.en.html
 * . Use of PauWare is more precisely ruled by the Creative Commons Legal Code defining the "Attribution-NonCommercial-ShareAlike 2.5" license: http://www.gnu.org/licenses/lgpl-3.0.en.htmlby-nc-sa/2.5/legalcode
 */
package com.pauware.pauware_engine._Exception;

/**
 * This class creates a common supertype for the {@link Statechart_state_based_exception}, {@link Statechart_transition_based_exception} and {@link Statechart_execution_based_exception} classes.
 * <p>
 * Compatibility: <I>PauWare Java EE</I> (Java SE/J2EE) and <I>PauWare Java ME</I> (Java SE/Java ME).
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public class Statechart_exception extends Exception {

    /**
     * @param message A text explaining the problem's cause.
     */
    public Statechart_exception(String message) {
        super(message);
    }
}