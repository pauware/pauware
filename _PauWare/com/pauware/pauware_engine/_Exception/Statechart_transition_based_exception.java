/**
 * PauWare engine software (http://www.PauWare.com)
 * InterDeposit Digital Number IDDN.FR.001.360023.000.S.P.2006.000.10000
 * . Use of this software is subject to the restrictions of the LGPL license version 3 http://www.gnu.org/licenses/lgpl-3.0.en.html
 * . Use of PauWare is more precisely ruled by the Creative Commons Legal Code defining the "Attribution-NonCommercial-ShareAlike 2.5" license: http://www.gnu.org/licenses/lgpl-3.0.en.htmlby-nc-sa/2.5/legalcode
 */
package com.pauware.pauware_engine._Exception;

import com.pauware.pauware_engine._Core.AbstractStatechart;

/**
 * This class is concerned with abnormal transition configuration/structuring exceptions.
 * It is for instance thrown when declared transitions are ill-formed, <I>e.g.</I>, a transition from a state to its direct superstate.
 * It is also for instance thrown when a transition conflicts with another one.
 * <p>
 * Compatibility: <I>PauWare Java EE</I> (Java SE/J2EE) and <I>PauWare Java ME</I> (Java SE/Java ME).
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 * @see com.pauware.pauware_engine._Core.AbstractStatechart_monitor#fires(String,AbstractStatechart,AbstractStatechart,Object,String,Object[],Object,String,Object[],byte)
 */
public class Statechart_transition_based_exception extends Statechart_exception {

    /**
     * This field refers to the source state of the transition causing the problem.
     */
    AbstractStatechart _from;
    /**
     * This field refers to the end state of the transition causing the problem.
     */
    AbstractStatechart _to;

    /**
     * @param message A text explaining the problem's cause.
     * @param from The source state of the transition causing the problem.
     * @param to The end state of the transition causing the problem.
     */
    public Statechart_transition_based_exception(String message, AbstractStatechart from, AbstractStatechart to) {
        super(message);
        _from = from;
        _to = to;
    }

    /**
     * @return A message explaning why a transition is ill-formed or is a source of conflict.
     */
    public String getMessage() {
        if (_from == null) {
            if (_to == null) {
                return super.getMessage() + ": 'from' and 'to' are both 'null'";
            }
            return super.getMessage() + ": 'from' is 'null' and 'to' is " + _to.name();
        }
        if (_to == null) {
            return super.getMessage() + ": 'from' is " + _from.name() + " and 'to' is 'null'";
        }
        return super.getMessage() + ": 'from' is " + _from.name() + " and 'to' is " + _to.name();
    }
}