/**
 * PauWare engine software (http://www.PauWare.com)
 * InterDeposit Digital Number IDDN.FR.001.360023.000.S.P.2006.000.10000
 * . Use of this software is subject to the restrictions of the LGPL license version 3 http://www.gnu.org/licenses/lgpl-3.0.en.html
 * . Use of PauWare is more precisely ruled by the Creative Commons Legal Code defining the "Attribution-NonCommercial-ShareAlike 2.5" license: http://www.gnu.org/licenses/lgpl-3.0.en.htmlby-nc-sa/2.5/legalcode
 */
package com.pauware.pauware_engine._Exception;

/**
 * This class is concerned with abnormal situations or failures when the <I>PauWare</I> engine is running.
 * It acts as an encapsulation means for all kinds of problems that may occur in Java.
 * Any problem that may occur during a run-to-completion cycle is identified in creating an instance of this exception class.
 * <p>
 * Compatibility: <I>PauWare Java EE</I> (Java SE/J2EE) and <I>PauWare Java ME</I> (Java SE/Java ME).
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 * @see java.lang.Throwable
 * @see com.pauware.pauware_engine._Core.AbstractStatechart_monitor#run_to_completion(String,boolean)
 */
public class Statechart_execution_based_exception extends Statechart_exception {

    /**
     * This field refers to the problem creating the exception.
     */
    Object _problem;

    /**
     * @param message A text explaining the problem's cause.
     * @param problem The source object of the problem.
     */
    public Statechart_execution_based_exception(String message, Object problem) {
        super(message);
        _problem = problem;
    }

    /**
     * @return A message explaning why a transition is ill-formed.
     */
    public String getMessage() {
        if (_problem == null) {
            return super.getMessage();
        }
        if (_problem instanceof Throwable) {
            return ((Throwable) _problem).getMessage();
        }
        return super.getMessage() + ": " + _problem.toString();
    }
}


