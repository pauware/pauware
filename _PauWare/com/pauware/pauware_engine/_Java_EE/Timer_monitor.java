/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Java_EE;

import com.pauware.pauware_engine._Core.AbstractTimer_monitor;
import com.pauware.pauware_engine._Exception.Statechart_exception;

/**
 * This abstract class represents the notion of <I>timer services</I>.
 * <p>
 * A
 * software component may inherit from this abstract class in order to process
 * (react to) <I>timer events</I>, see:
 * {@link AbstractTimer_monitor#time_out(long,AbstractStatechart)}. In
 * <I>PauWare</I>, timers are cyclic; They stop on demand only, see:
 * {@link AbstractTimer_monitor#to_be_killed()}, {@link AbstractTimer_monitor#to_be_killed(AbstractStatechart)}
 * or {@link Timer_monitor#to_be_killed(Statechart)}.
 * <p>
 * Compatibility:
 * <I>PauWare Java EE</I> (Java SE/Java EE). <b>Caution</b>: this class <b>is
 * mandatory</b> if <I>timer services</I> are requested by means of reflection.
 * Best practices show that the <CODE>AbstractTimer_monitor</CODE> class is
 * better for <I>PauWare Java ME</I>, while the <CODE>Timer_monitor</CODE> class
 * is better for <I>PauWare Java EE</I>.
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
abstract public class Timer_monitor extends AbstractTimer_monitor {

    /**
     * This method is the same as
     * {@link AbstractTimer_monitor#to_be_killed(AbstractStatechart)}.
     */
    public void to_be_killed(final Statechart context) {
        super.to_be_killed(context);
    }

    /**
     * This method is the same as
     * {@link AbstractTimer_monitor#to_be_reset(AbstractStatechart,long)}.
     */
    public void to_be_reset(final Statechart context, final long delay) throws Statechart_exception {
        super.to_be_reset(context, delay);
    }

    /**
     * This method is the same as
     * {@link AbstractTimer_monitor#to_be_reset(AbstractStatechart,Long)}.
     */
    public void to_be_reset(final Statechart context, final Long delay) throws Statechart_exception {
        super.to_be_reset(context, delay);
    }

    /**
     * This method is the same as
     * {@link AbstractTimer_monitor#to_be_set(AbstractStatechart,long)}.
     */
    public void to_be_set(final Statechart context, final long delay) throws Statechart_exception {
        super.to_be_set(context, delay);
    }

    /**
     * This method is the same as
     * {@link AbstractTimer_monitor#to_be_set(AbstractStatechart,Long)}.
     */
    public void to_be_set(final Statechart context, final Long delay) throws Statechart_exception {
        super.to_be_set(context, delay);
    }
}
