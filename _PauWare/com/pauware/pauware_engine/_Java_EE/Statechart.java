/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Java_EE;

import com.pauware.pauware_engine._Core.*;
import com.pauware.pauware_engine._Java_ME.Java_MEDo_activity;

/**
 * This concrete class represents the general notion of <I>State</I> in UML.
 * <p>
 * Compatibility: <I>PauWare Java EE</I> (Java SE/Java EE).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public class Statechart extends AbstractStatechart implements java.io.Serializable {

    /**
     * @see AbstractStatechart#AbstractStatechart(String)
     */
    /*public Statechart(String name) {
        super(name);
    }*/
    
    public Statechart(String name) {
        super(name);
    }

    /**
     * This method creates and returns an action instance that is compatible
     * with the chosen platform; This is an instance of {@link Action} if the
     * <CODE>reentrance_mode</CODE> parameter is equal to
     * <CODE>AbstractStatechart.No_reentrance</CODE>; Otherwise, it is an
     * instance of {@link SendSignalAction}.
     */
    protected AbstractAction action(Object object, String action, Object[] args, byte reentrance_mode) {
        switch (reentrance_mode) {
            case No_reentrance:
                return new Action(object, action, args);
            case Reentrance:
                return new SendSignalAction(object, action, args);
            default:
                return new SendSignalAction(object, action, args);
        }
    }

    /**
     * This method creates and returns an activity instance that is compatible
     * with the chosen platform; This is an instance of {@link Do_activity}.
     */
    protected AbstractAction activity(Object object, String action, Object[] args) {
        return new Do_activity(object, action, args);
    }

    /**
     * This method creates and returns a guard instance that is compatible with
     * the chosen platform; This is an instance of {@link Guard}.
     */
    protected AbstractGuard guard(Object guard_object, String guard_action, Object[] guard_args) {
        return new Guard(guard_object, guard_action, guard_args);
    }

    /**
     * This method creates and returns a superstate instance that is compatible
     * with the chosen platform; This is an instance of {@link Statechart}.
     */
    protected AbstractStatechart createFather() {
        return new Statechart(null);
    }
}
