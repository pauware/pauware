package com.pauware.pauware_engine._Core;

import com.pauware.pauware_engine._Exception.Statechart_exception;

/**
 * This interface has to be implemented to monitor or observe the execution of a state machine.
 * A specific method of the interface is called on the monitor depending of what is currently processed on the state machine.
 * @author Lea, Eric
 */
public interface Observable {
        
    /**
     * This operation is called when a state is activated
     * @param state the activited state
     */
    void onStateEntry(AbstractStatechart state);
    
    /**
     * This operation is called when a state is desactivated
     * @param state the desactivited state
     */
    void onStateExit(AbstractStatechart state);
    
    /**
     * This operation is called when an entry action is executed on a state
     * @param state the state owning the action
     * @param action the executed action
     */
    void onEntryAction(AbstractStatechart state, AbstractAction action);

    /**
     * This operation is called when an exit action is executed on a state
     * @param state the state owning the action
     * @param action the executed action
     */
    void onExitAction(AbstractStatechart state, AbstractAction action);
    
    /**
     * This operation is called when a do operation is executed on a state
     * @param state the state owning the action
     * @param action the executed action
     */
    void onDoActivity(AbstractStatechart state, AbstractAction action);
    
    /**
     * This operation is called when the PauWare engine processes an allowed event on a state
     * @param state the state owning the action
     * @param action the executed action
     */
    void onAllowedEvent(AbstractStatechart state, AbstractAction action); 
    
    /**
     * This operation is called when an invariant is violated
     * @param state the state owning the invariant
     * @param invariant the operation implementing the invariant
     */
    void onViolatedInvariant(AbstractStatechart state, AbstractAction invariant);
    
    /**
     * This operation is called when an invariant is verified
     * @param state the state owning the invariant
     * @param invariant the operation implementing the invariant
     */
    void onVerifiedInvariant(AbstractStatechart state, AbstractAction invariant);
    
    /**
     * This operation is called when the state machine is started
     * @param stateMachine the started state machine
     */
    void onStateMachineStart(AbstractStatechart stateMachine);
    
    /**
     * This operation is called when the state machine is stopped
     * @param stateMachine the stopped state machine
     */
    void onStateMachineStop(AbstractStatechart stateMachine);
    
    /**
     * This operation is called when a guard is evaluated as true on a triggerable transition
     * @param transition the transition owning the guard
     * @param guard the operation implementing the guard
     */
    void onGuardValid(Transition transition, AbstractGuard guard); 
    
    /**
     * This operation is called when a guard is evaluated as false on a triggerable transition
     * @param transition the transition owning the guard
     * @param guard the operation implementing the guard
     */
    void onGuardUnvalid(Transition transition, AbstractGuard guard);
    
    /**
     * This operation is called when an action is executed when triggering a transition
     * @param transition the transition owning the operation
     * @param action the executed operation
     */
    void onTransitionOperation(Transition transition, AbstractAction action);
    
    /**
     * This operation is called when a transition is triggered
     * @param transition the fired transition
     */
    void onFiredTransition(Transition transition);
    
    /**
     * This operation is called when a "run to completion" cycle is started
     * @param event the event processed during the cycle
     */
    void startCompletionCycle(String event);
    
    /**
     * This opertion is called when a "run to completion" cycle has ended
     * @param event the event processed during the cycle
     */
    void endCompletionCycle(String event);
    
    /**
     * This operation is called when an error occurs during the state machine execution 
     * @param err the occured error
     */
    void onError(Statechart_exception err);
    
    /**
     * This method has to be called if the monitor need to have a reference on the state machine 
     * @param sm the PauWare state machine monitor
     */
    void setStateMachine(AbstractStatechart_monitor sm);
}
