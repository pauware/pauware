/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pauware.pauware_engine._Core;

/**
 * The definition of the Java platform: Java EE or Java ME
 * @author Eric
 */
public enum JavaPlatform {
    JavaEE, JavaME;
}
