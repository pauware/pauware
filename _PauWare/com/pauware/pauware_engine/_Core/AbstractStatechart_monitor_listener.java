/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Core;

/**
 * This interface is a management utility (including visualization issues), which can
 * be implemented by a manager object. This object is intended to get
 * information about the discrete evolution of a state machine. It provides
 * basic services so that this manager object is able to be informed of cyclic
 * changes coming from the managed component (<I>i.e.</I>, that owning the state machine).
 * <p>
 * Compatibility:
 * <I>PauWare Java EE</I> (Java SE/Java EE) and <I>PauWare Java ME</I> (Java
 * SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public interface AbstractStatechart_monitor_listener {

    /**
     * This method communicates the initial structure of a state machine. Such a
     * structure includes all of the states, their relationships and the cached
     * transitions, <I>i.e.</I>, those declared before calling the
     * {@link AbstractStatechart_monitor#start()} method. Transitions may be
     * accessed through the {@link AbstractStatechart_monitor#transitions()}
     * method.
     *
     * @param state_machine the state machine itself.
     */
    void post_construct(final AbstractStatechart_monitor state_machine) throws Exception;

    /**
     * This method is called when entering an entire state machine; it is called
     * within the {@link AbstractStatechart_monitor#start()} method.
     *
     * @param verbose A string statingwhat happened.
     */
    void start(final String verbose) throws Exception;

    /**
     * This method communicates the detailed result of an execution (trace) in a
     * verbose mode just after a state machine execution; it also communicates
     * the effective triggered transitions. This occurs after a
     * run-to-completion cycle.
     *
     * @param verbose A string stating what happened.
     * @param execution The set of {@link Transition} objects (keys) and their
     * associated actions (values are Java primitive arrays of
     * {@link AbstractAction}.
     */
    void run_to_completion(final String verbose, final java.util.Hashtable execution) throws Exception;

    /**
     * This method is called when exiting an entire state machine; it is called
     * within the {@link AbstractStatechart_monitor#stop()} method.
     *
     * @param verbose A string stating what happened.
     */
    void stop(final String verbose) throws Exception;

    /**
     * This method is called after a state machine has been stopped.
     */
    void pre_destroy() throws Exception;
}
