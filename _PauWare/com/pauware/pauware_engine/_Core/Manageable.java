/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Core;

import com.pauware.pauware_engine._Exception.Statechart_exception;

/**
 * This interface is a management utility, which can be implemented by a
 * software component so that it can be accessed by means of Java Management
 * eXtensions (JMX) in particular. <p> Compatibility: <I>PauWare Java EE</I>
 * (Java SE/Java EE) and <I>PauWare Java ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public interface Manageable extends Manageable_base {

    /**
     * This method forces a software component to move to a given state, thus
     * forcing its orthogonal states and superstates to be also active. <p> Its
     * typical implementation is as follows:
     * <CODE>_my_state_machine.to_state("A state");</CODE> where the type of
     * the
     * <CODE>_my_state_machine</CODE> field in the component is
     * {@link AbstractStatechart_monitor}. <b>Caution</b>: this method triggers
     * the <I>entry</I> and <I>exit</I> actions associated with states.
     *
     * @see AbstractStatechart#set_entryAction(Object,String,Object[],byte)
     * @see AbstractStatechart#set_exitAction(Object,String,Object[],byte)
     * @see AbstractStatechart#reset_entryAction(Object,String,Object[],byte)
     * @see AbstractStatechart#reset_exitAction(Object,String,Object[],byte)
     */
    void to_state(String name) throws Statechart_exception;
}