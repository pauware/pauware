/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Core;

/**
 * This interface is a utility which can be implemented by a software component
 * to create an identifier. <p> Compatibility: <I>PauWare Java EE</I> (Java
 * SE/Java EE) and <I>PauWare Java ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public interface Unique {

    /**
     * This method returns the identifier of a component. <p> An appropriate
     * approach is to implement this method in compliance with the
     * {@link java.lang.Object#equals(Object)} and
     * {@link java.lang.Object#hashCode()} methods.
     */
    String unique();
}