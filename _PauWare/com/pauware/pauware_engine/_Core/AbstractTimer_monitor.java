/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Core;

import java.util.Timer;
import java.util.TimerTask;

import com.pauware.pauware_engine._Exception.Statechart_exception;

/**
 * This abstract class represents the notion of <I>timer services</I>. <p> A
 * software component may inherit from this abstract class in order to process
 * (react to) <I>timer events</I> (see:
 * {@link AbstractTimer_monitor#time_out(long,AbstractStatechart)}). In
 * <I>PauWare</I>, timers are cyclic; They stop on demand only (see:
 * {@link AbstractTimer_monitor#to_be_killed()} or
 * {@link AbstractTimer_monitor#to_be_killed(AbstractStatechart)}). <p>
 * Compatibility: <I>PauWare Java EE</I> (Java SE/Java EE) and <I>PauWare Java
 * ME</I>
 * (Java SE/Java ME). <b>Caution</b>: this class <b>must not</b> be used if
 * <I>timer services</I> are requested by means of reflection. In UML, this
 * amounts for instance to the following formalisms: <I>entry/ to be
 * set(1000)</I>, <I>exit/ to be killed</I> or <I>event[guard]/ to be
 * set(1000)</I>. In case of reflection (<I>PauWare Java EE</I>, Java SE/Java
 * EE), use {@link com.pauware.pauware_engine._Java_EE.Timer_monitor} instead.
 * Best practices show that
 * <CODE>AbstractTimer_monitor</CODE> is preferable for <I>PauWare Java ME</I>
 * while
 * <CODE>Timer_monitor</CODE> is better for <I>PauWare Java EE</I>.
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
abstract public class AbstractTimer_monitor implements Timed_component {

    /**
     * This field records the timers associated with <b>non</b>
     * <CODE>null</CODE> contexts. Contexts are keys while timers are values of
     * this
     * <CODE>Hashtable</CODE> instance.
     */
    private java.util.Hashtable _contexts = new java.util.Hashtable();
    /**
     * This field records the last problem (if any) resulting from the use of
     * timer services.
     *
     * @see AbstractTimer_monitor#time_out_error(Statechart_exception)
     */
    private Statechart_exception _se = null;
    /**
     * This field represents the timer associated with the
     * <CODE>null</CODE> context.
     */
    private Timer _timer = null;

    /**
     * This method amounts to calling
     * <CODE>to_be_killed(null);</CODE>. <p> Killing the unique timer (if any)
     * associated with the
     * <CODE>null</CODE> context, does not lead to killing the other timers.
     */
    public void to_be_killed() {
        if (_timer == null) {
            return;
        }
        _timer.cancel();
        _timer = null;
    }

    /**
     * This method kills the unique timer (if any) associated with the
     * <CODE>context</CODE> parameter. <p> Killing the unique timer (if any)
     * associated with the
     * <CODE>context</CODE> parameter, does not lead to killing the other
     * timers, including that associated with the
     * <CODE>null</CODE> context.
     */
    public void to_be_killed(final AbstractStatechart context) {
        Timer timer = (Timer) _contexts.get(context);
        if (timer == null) {
            return;
        }
        timer.cancel();
        _contexts.remove(context);
    }

    /**
     * This method amounts to calling
     * <CODE>to_be_reset(null,delay);</CODE>. <p> Only one timer may exist at a
     * time which has the
     * <CODE>null</CODE> context.
     */
    public void to_be_reset(final long delay) throws Statechart_exception {
        to_be_killed();
        to_be_set(delay);
    }

    /**
     * This method is the same as
     * {@link AbstractTimer_monitor#to_be_reset(long)}; It guarantees
     * compatibility with Java SE 1.4.x.
     */
    public void to_be_reset(final Long delay) throws Statechart_exception {
        to_be_reset(delay.longValue());
    }

    /**
     * This method amounts to killing the unique timer (if any) associated with
     * the
     * <CODE>context</CODE> parameter and next setting up this timer with the
     * new
     * <CODE>delay</CODE> parameter. <p> Only one timer may exist at a time
     * having the
     * <CODE>context</CODE> parameter. If the context does not exist (the
     * <CODE>null</CODE> context may be used as parameter), no exception is
     * raised.
     *
     * @see AbstractTimer_monitor#to_be_killed(AbstractStatechart)
     * @see AbstractTimer_monitor#to_be_set(AbstractStatechart,long)
     * @throws Statechart_exception An encapsulation of a possible exception
     * raised by the {@link java.util.Timer} or {@link java.util.TimerTask}
     * classes
     */
    public void to_be_reset(final AbstractStatechart context, final long delay) throws Statechart_exception {
        to_be_killed(context);
        to_be_set(context, delay);
    }

    /**
     * This method is the same as
     * {@link AbstractTimer_monitor#to_be_reset(AbstractStatechart,long)}; It
     * guarantees compatibility with Java SE 1.4.x.
     */
    public void to_be_reset(final AbstractStatechart context, final Long delay) throws Statechart_exception {
        to_be_reset(context, delay.longValue());
    }

    /**
     * This method amounts to calling
     * <CODE>to_be_set(null,delay);</CODE>.
     */
    public void to_be_set(final long delay) throws Statechart_exception {
        _se = null;
        if (_timer != null) {
            return;
        }
        TimerTask timer_task = new TimerTask() {
            public void run() {
                try {
                    if (_se != null) {
                        to_be_killed();
                        time_out_error(_se);
                    } else {
                        time_out(delay, null);
                    }
                } catch (Statechart_exception se) {
                    _se = se;
                }
            }
        };
        _timer = new Timer();
        try {
            _timer.schedule(timer_task, delay, delay);
        } catch (IllegalArgumentException iae) {
            throw new Statechart_exception(iae.toString() + ": " + delay);
        } catch (IllegalStateException ise) {
            throw new Statechart_exception(ise.toString() + ": " + delay);
        }
    }

    /**
     * This method is the same as {@link AbstractTimer_monitor#to_be_set(long)};
     * It guarantees compatibility with Java SE 1.4.x.
     */
    public void to_be_set(final Long delay) throws Statechart_exception {
        to_be_set(delay.longValue());
    }

    /**
     * This method is called by a software component when <I>timer services</I>
     * are required. <p> The
     * <CODE>context</CODE> parameter may be
     * <CODE>null</CODE>. In any case, calling this method while a timer is
     * already assigned to the
     * <CODE>context</CODE> parameter has no effect; So, use
     * {@link AbstractTimer_monitor#to_be_reset(AbstractStatechart,long)}
     * instead. Otherwise (in case of success), a software component will
     * therefore receive, cyclically, a <I>time-out</I> event each time, the
     * <CODE>delay</CODE> parameter is elapsed.
     *
     * @see
     * com.pauware.pauware_engine._Core.AbstractTimer_monitor#time_out(long,AbstractStatechart)
     */
    public void to_be_set(final AbstractStatechart context, final long delay) throws Statechart_exception {
        _se = null;
        if (_contexts.containsKey(context)) {
            return;
        }
        TimerTask timer_task = new TimerTask() {
            public void run() {
                try {
                    if (_se != null) {
                        to_be_killed(context);
                        time_out_error(_se);
                    } else {
                        time_out(delay, context);
                    }
                } catch (Statechart_exception se) {
                    _se = se;
                }
            }
        };
        Timer timer = new Timer();
        _contexts.put(context, timer);
        try {
            timer.schedule(timer_task, delay, delay);
        } catch (IllegalArgumentException iae) {
            throw new Statechart_exception(iae.toString() + ": " + context._name + "-" + delay);
        } catch (IllegalStateException ise) {
            throw new Statechart_exception(ise.toString() + ": " + context._name + "-" + delay);
        }
    }

    /**
     * This method is the same as
     * {@link AbstractTimer_monitor#to_be_set(AbstractStatechart,long)}; It
     * guarantees compatibility with Java SE 1.4.x.
     */
    public void to_be_set(final AbstractStatechart context, final Long delay) throws Statechart_exception {
        to_be_set(context, delay.longValue());
    }

    /**
     * @throws Statechart_exception An encapsulation of a possible exception
     * raised by the
     * {@link com.pauware.pauware_engine._Core.AbstractStatechart_monitor#fires(String,AbstractStatechart,AbstractStatechart,Object,String,Object[],Object,String,Object[],byte)}
     * or
     * {@link com.pauware.pauware_engine._Core.AbstractStatechart_monitor#run_to_completion(String,boolean)}
     * methods
     */
    abstract public void time_out(long delay, AbstractStatechart context) throws Statechart_exception;

    /**
     * @throws Statechart_exception Having this exception type in the signature
     * allows the use of methods themselves raising such an exception type
     */
    abstract public void time_out_error(Statechart_exception se) throws Statechart_exception;
}
