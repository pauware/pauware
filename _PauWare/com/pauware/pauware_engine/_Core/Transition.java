/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Core;

import com.pauware.pauware_engine._Exception.Statechart_transition_based_exception;

/**
 * This class represents the general notion of <I>Transition</I> in UML.
 * <p>
 * Compatibility: <I>PauWare Java EE</I> (Java SE/Java EE) and <I>PauWare Java
 * ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 * 
 * Extended by Eric Cariou
 */
public class Transition {

    /**
     * The state at the source of the transition.
     */
    protected final AbstractStatechart _from;
    /**
     * The state at the end of the transition.
     */
    protected final AbstractStatechart _to;
   
    /**
     * The event of the transition
     */
    protected String event;
    
    /**
     * The object on which the guard operation is executed
     */
    protected Object guardObject = null;
    
    /**
     * The name of the guard operation 
     */
    protected String guardMethodName = null;
    
    /** 
     * The array of the parameters of the guard operation
     */
    protected Object[] guardParams = null;
    
    /**
     * The object on which the business operation is executed
     */
    protected Object businessObject = null;
    
    /**
     * The name of the business operation 
     */
    protected String businessMethodName = null;
    
    /** 
     * The array of the parameters of the business operation
     */
    protected Object[] businessParams = null;
    
    /**
     * @return the event of the transition
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event set the event of the transition
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the object on which the guard is called
     */
    public Object getGuardObject() {
        return guardObject;
    }

    /**
     * @param guardObject set the object on which the guard is called
     */
    public void setGuardObject(Object guardObject) {
        this.guardObject = guardObject;
    }

    /**
     * @return the name of the guard method
     */
    public String getGuardMethodName() {
        return guardMethodName;
    }

    /**
     * @param guardMethodName set the name of the guard method
     */
    public void setGuardMethodName(String guardMethodName) {
        this.guardMethodName = guardMethodName;
    }

    /**
     * @return the parameters of the guard method
     */
    public Object[] getGuardParams() {
        return guardParams;
    }

    /**
     * @param guardParams set or change the parameters of the guard method
     */
    public void setGuardParams(Object... guardParams) {
        this.guardParams = guardParams;
    }

    /**
     * @return the object on which the business method is called
     */
    public Object getBusinessObject() {
        return businessObject;
    }

    /**
     * @param businessObject set the object on which the business method is called
     */
    public void setBusinessObject(Object businessObject) {
        this.businessObject = businessObject;
    }

    /**
     * @return the name of the business operation
     */
    public String getBusinessMethodName() {
        return businessMethodName;
    }

    /**
     * @param businessMethodName set the name of the business operation
     */
    public void setBusinessMethodName(String businessMethodName) {
        this.businessMethodName = businessMethodName;
    }

    /**
     * @return the parameters of the business operation
     */
    public Object[] getBusinessParams() {
        return businessParams;
    }

    /**
     * @param businessParams set or change the parameters of the business operation
     */
    public void setBusinessParams(Object... businessParams) {
        this.businessParams = businessParams;
    }
    
    /**
     * Initialize a transition. If a business operation or a guard has been set, their parameters must also be set 
     * with initial values for setting the complete signature of the operation.
     * @param stateMachine
     * @throws Statechart_transition_based_exception in case of problem
     */
    public void initializeTransition(AbstractStatechart_monitor stateMachine) throws Statechart_transition_based_exception {
        //System.out.println(" ***** on fait un fires");
        this.modifyTransition(stateMachine);
    }
    
    /**
     * @return true if a business operation is defined on the transition
     */
    protected boolean hasBusinessOperation() {
        if ((businessObject!=null) && (businessMethodName!=null)) return true;
        else return false;
    }
    
    /**
     * @return true if a guard is defined on the transition
     */
    protected boolean hasGuard() {
        if ((guardObject!=null) && (guardMethodName!=null)) return true;
        else return false;
    }
    
    /**
     * If the transition has been modified, typically when the parameters of the business operation or the guard have been modified,
     * this method has to be called for updating the transition.
     * @param stateMachine the state machine owning the transition
     * @throws Statechart_transition_based_exception in case of problem
     */
    public void modifyTransition(AbstractStatechart_monitor stateMachine) throws Statechart_transition_based_exception {
        //stateMachine.fires(event,_from, _to, guardObject, guardMethodName, guardParams, businessObject, businessMethodName, businessParams);
        if ( ! hasGuard() ) {
           if ( ! hasBusinessOperation() )
               // no guard and no operation
               stateMachine.fires(event, _from, _to);
           else 
               // no guard and an operation
               stateMachine.fires(event, _from, _to, true, businessObject, businessMethodName, businessParams);
           }
        else if ( ! hasBusinessOperation() ) 
             // a guard and no operation
             stateMachine.fires(event, _from, _to, guardObject, guardMethodName, guardParams);
        else
            // a guard and an operation
            stateMachine.fires(event, _from, _to, guardObject, guardMethodName, guardParams, businessObject, businessMethodName, businessParams);
    }
   
    protected Transition(AbstractStatechart from, AbstractStatechart to) {
        this._from = from;
        this._to = to;
        this.event = "completion";
    }
    
    /**
     * Create a basic transition that can be afterwads extended with a guard or business operation definition
     * @param event the event of the transition
     * @param from the source state of the transition
     * @param to the targer state of the transition
     */
    public Transition(String event,  AbstractStatechart from, AbstractStatechart to) {
 
        this._from = from;
        this._to = to;
        this.event = event;
    }

    /**
     * This method is used when a <I>Transition</I> is put in a hashtable as a
     * key.
     */
    public boolean equals(Object transition) {
        if (this == transition) {
            return true;
        }
        if (transition instanceof Transition) {
            return _from.equals(((Transition) transition)._from) && _to.equals(((Transition) transition)._to);
        }
        return false;
    }

    /**
     * This method is used when a <I>Transition</I> is put in a hashtable as a
     * key.
     */
    public int hashCode() {
        return _from.hashCode() * 31 + _to.hashCode();
    }

    /**
     * This method is an accessor for the {@link Transition#_from} field.
     */
    public AbstractStatechart from() {
        return _from;
    }

    /**
     * This method is an accessor for the {@link Transition#_to} field.
     */
    public AbstractStatechart to() {
        return _to;
    }
    
    /**
     * @return the source state of the transition
     */
    public AbstractStatechart getFrom() {
        return _from;
    }

     /**
     * @return the target state of the transition
     */
    public AbstractStatechart getTo() {
        return _to;
    }
    
}
