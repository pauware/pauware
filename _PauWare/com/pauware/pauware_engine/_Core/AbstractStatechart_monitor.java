/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Core;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Vector;

import com.pauware.pauware_engine._Exception.*;

/**
 * This abstract class represents the general notion of <I>State Machine</I> in
 * UML. Use of this class occurs through the following subclasses:
 * {@link com.pauware.pauware_engine._Java_EE.Statechart_monitor} (Java SE/Java
 * EE) or {@link com.pauware.pauware_engine._Java_ME.Java_MEStatechart_monitor}
 * (Java SE/Java ME).
 * <p>
 * Compatibility: <I>PauWare Java EE</I> (Java SE/Java EE) and <I>PauWare Java
 * ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
abstract public class AbstractStatechart_monitor extends AbstractStatechart implements Manageable {

    /**
     * This class variable is a constant value that disactivates the calculation
     * of state invariants.
     *
     * @see
     * com.pauware.pauware_engine._Core.AbstractStatechart_monitor#run_to_completion(String,boolean)
     */
    public static final boolean Don_t_compute_invariants = false;
    /**
     * This class variable is a constant value that activates the calculation of
     * state invariants.
     *
     * @see
     * com.pauware.pauware_engine._Core.AbstractStatechart_monitor#run_to_completion(String,boolean)
     */
    public static final boolean Compute_invariants = true;
    /**
     * This class variable is a constant value which represents the fact that
     * the verbose mode for event processing is not activated.
     *
     * @see
     * AbstractStatechart_monitor#AbstractStatechart_monitor(AbstractStatechart,String,boolean)
     */
    public static final boolean Don_t_show_on_system_out = false;
    /**
     * This class variable is a constant value which represents the fact that
     * the verbose mode for event processing is activated.
     *
     * @see
     * AbstractStatechart_monitor#AbstractStatechart_monitor(AbstractStatechart,String,boolean)
     */
    public static final boolean Show_on_system_out = true;
    /**
     * This field records the chosen verbose mode associated with the state
     * machine.
     *
     * @see AbstractStatechart_monitor#Don_t_show_on_system_out
     * @see AbstractStatechart_monitor#Show_on_system_out
     */
    protected boolean _show_on_system_out = Don_t_show_on_system_out;
    /**
     * This field represents all of the eligible transitions of a given
     * run-to-completion cycle. This <CODE>Hashtable</CODE> instance is empty at
     * the beginning of a run-to-completion cycle.
     */
    protected Hashtable _execution = new Hashtable();
    /**
     * This field records, during a run-to-completion cycle, the transitions to
     * be eliminated. Eliminated transitions are the transitions overridden by
     * other similar ones bound to substates. They are also those inhibited by
     * allowed events.
     */
    protected Vector _eliminated_transitions = new Vector();
    /**
     * This field represents all of the cached transitions of the state machine.
     */
    protected Hashtable _transitions = new Hashtable();
    /**
     * This field records a listener, if any, for the software component that
     * owns the state machine.
     */
    protected AbstractStatechart_monitor_listener _listener = null;

    
    /** Set a transition for the state machine (Eric Cariou)
     * @param t the transition to add to the state machine
     * @throws com.pauware.pauware_engine._Exception.Statechart_transition_based_exception in case of problem
     */
    public void setTransition(Transition t) throws Statechart_transition_based_exception {
        t.initializeTransition(this);
    }
    
    /**
     * This method adds a listener to the software component that owns the state
     * machine. A typical use of this method is when a viewer is added in order
     * to graphically simulate a state machine as follows:
     * <CODE>com.pauware.pauware_view.PauWare_view pv = new com.pauware.pauware_view.PauWare_view();</CODE>
     * <CODE>_Programmable_thermostat.add_listener(pv);</CODE>
     * <CODE>_Programmable_thermostat.initialize_listener();</CODE>
     *
     * @see AbstractStatechart_monitor#initialize_listener()
     * @see AbstractStatechart_monitor#remove_listener()
     */
    synchronized public void add_listener(AbstractStatechart_monitor_listener listener) {
        if (_listener == null) {
            _listener = listener;
        }
    }

    /**
     * This method supplies a listener with the state machine of a software
     * component.
     *
     *
     * @see
     * AbstractStatechart_monitor#add_listener(AbstractStatechart_monitor_listener)
     */
    synchronized public void initialize_listener() throws Statechart_exception {
        try {
            if (_listener != null) {
                _listener.post_construct(this);
            }
        } catch (Exception e) {
            throw new Statechart_exception(e.getMessage());
        }
    }

    /**
     * This method removes the current listener (if any) of the software
     * component that owns the state machine.
     *
     * @see
     * AbstractStatechart_monitor#add_listener(AbstractStatechart_monitor_listener)
     */
    synchronized public void remove_listener() throws Statechart_exception {
        try {
            if (_listener != null) {
                _listener.pre_destroy();
                _listener = null;
            }
        } catch (Exception e) {
            throw new Statechart_exception(e.getMessage());
        }
    }

    /**
     * This field records the state of a state machine computed within of a
     * run-to-completion step.
     *
     * @see Manageable_base#async_current_state()
     * @see Manageable_base#current_state()
     */
    protected String _current_state = "Not yet defined";
    /**
     * This field records the result of an event occurrence processing within a
     * run-to-completion step.
     *
     * @see AbstractStatechart_monitor#Don_t_show_on_system_out
     * @see AbstractStatechart_monitor#Show_on_system_out
     */
    protected StringBuffer _verbose = new StringBuffer();

    /**
     * This constructor creates a father for two composed
     * <CODE>AbstractStatechart_monitor</CODE> instances. Since ver. 1.2
     * (vertical composition).
     *
     * @see AbstractStatechart#createFather()
     */
    protected AbstractStatechart_monitor() {
        super(null);
    }

    /**
     * This constructor amounts to calling
     * <CODE>AbstractStatechart_monitor(s,name,AbstractStatechart_monitor.Don_t_show_on_system_out)</CODE>.
     * It supports the composition of <CODE>AbstractStatechart_monitor</CODE>
     * instances (a.k.a. <I>vertical composition</I>) since ver. 1.2.
     */
    public AbstractStatechart_monitor(AbstractStatechart s, String name) throws Statechart_exception {
        super(name);
        if (s == null) {
            throw new Statechart_exception("AbstractStatechart_monitor initialization with 'null' value");
            // _node == this; // not compatible with 'commonSuperWith'
        }
        _left = s._left;
        _right = s._right;
        s._left._node = this;
        s._right._node = this;
        _xor = s._xor;
        /**
         * Completion transitions (i.e., those without any event label) are
         * associated with output states. Since ver. 1.3.
         */
        set_completion(this);
        // Cyril:
        // if(s instanceof AbstractStatechart_monitor) mergeVariables((AbstractStatechart_monitor)s);
    }

    /**
     * Completion transitions (i.e., those without any event label) are
     * automatically triggered when reaching output states. Since ver. 1.3.
     */
    public void completion() throws Statechart_exception {
        run_to_completion(Completion);
    }

    /**
     * This constructor first creates and initializes a state machine; It next
     * triggers all <I>entry</I> actions associated with all the explicit and
     * implicit default input states of the state machine. Running this
     * constructor therefore implies that all resources, used by the state
     * machine in <I>entry</I> actions, are available and in an appropriate
     * status. . Use of this class occurs through the following subclasses:
     * {@link com.pauware.pauware_engine._Java_EE.Statechart_monitor}
     * (<I>PauWare Java EE</I> - Java SE/Java EE) or
     * {@link com.pauware.pauware_engine._Java_ME.Java_MEStatechart_monitor}
     * (<I>PauWare Java ME</I> - Java SE/Java ME). Example of a user-defined
     * <CODE>Stack</CODE> software component:
     * <p>
     * <CODE>
     * <br>AbstractStatechart_monitor _Stack;
     * <br>...
     * <br>_Stack = new Statechart_monitor(_Empty.xor(_Not_empty),"Stack");
     * </CODE>
     *
     * @param s A state object resulting from a XOR-based state composition or
     * an AND-based state composition
     * @param name A name assigned to the state machine. The name of the
     * software component type running this state machine is preferred for this
     * parameter
     * @param show_on_system_out If <CODE>true</CODE>, this parameter activates
     * the verbose mode of a run-to-completion cycle (for tests only)
     */
    public AbstractStatechart_monitor(AbstractStatechart s, String name, boolean show_on_system_out) throws Statechart_exception {
        this(s, name);
        _show_on_system_out = show_on_system_out;
    }

    /**
     * This constructor amounts to calling
     * <CODE>AbstractStatechart_monitor(s,name,show_on_system_out)</CODE> plus
     * the definition of a state machine's listener.
     *
     * @param listener The state machine's listener is first initiliazed in
     * receiving the state machine itself (for display purposes for instance)
     * Next, it will be informed of the state machine's status changes at the
     * time when each run-to-completion cycle is finished
     * @see
     * AbstractStatechart_monitor#AbstractStatechart_monitor(AbstractStatechart,String,boolean)
     * @see
     * AbstractStatechart_monitor#add_listener(AbstractStatechart_monitor_listener)
     * @see AbstractStatechart_monitor#initialize_listener()
     */
    public AbstractStatechart_monitor(AbstractStatechart s, String name, boolean show_on_system_out, AbstractStatechart_monitor_listener listener) throws Statechart_exception {
        this(s, name, show_on_system_out);
        add_listener(listener);
    }

    /**
     * Internal use only; This method is called during a run-to-completion
     * cycle.
     *
     * @see AbstractStatechart_monitor#run_to_completion(String,boolean)
     */
    protected void activate(AbstractStatechart from, AbstractStatechart to, StringBuffer verbose) throws Statechart_exception {
        AbstractStatechart commonSuper = from.commonSuperWith(to);
        Stack state_entry_flow = new Stack();
//        for (AbstractStatechart s = to._node; s != null && s != commonSuper && s._active == false; s = s._node) {
        for (AbstractStatechart s = to; s != null && s != commonSuper && s._active == false; s = s._node) {
            state_entry_flow.push(s);
        }
//        while (!superstates.empty()) {
//            ((AbstractStatechart) superstates.pop()).entry(false);
//        }
        if (!state_entry_flow.empty()) {
//            ((AbstractStatechart) superstates.pop()).entry(false);
            ((AbstractStatechart) state_entry_flow.pop()).entry(state_entry_flow, verbose);
        }
//        to.entry(true);
    }

    /**
     * Internal use only; This method is called during a run-to-completion
     * cycle.
     *
     * @see AbstractStatechart_monitor#run_to_completion(String,boolean)
     */
    protected void disactivate(AbstractStatechart from, AbstractStatechart to, StringBuffer verbose) throws Statechart_exception {
        AbstractStatechart commonSuper = from.commonSuperWith(to);
        to.exit(verbose);
        for (AbstractStatechart s = to; s != commonSuper; s = s._node) {
            if (s.isXorWith(s.brother())) {
                s.brother().exit(verbose);
            }
        }
    }

    /**
     * This method answers the question: is this state the most outer state of
     * the state machine? The answer is always <CODE>true</CODE> for an instance
     * of the <CODE>AbstractStatechart_monitor</CODE> type.
     */
    public boolean root() {
        return true;
    }

    /**
     * This method amounts to calling <CODE>fires(from,to,true);</CODE>.
     */
//    public void fires(AbstractStatechart from, AbstractStatechart to) throws Statechart_transition_based_exception {
//        fires(from, to, true);
//    }
    /**
     * This method amounts to calling <CODE>fires(event,from,to,true);</CODE>.
     */
    public void fires(String event, AbstractStatechart from, AbstractStatechart to) throws Statechart_transition_based_exception {
        fires(event, from, to, true);
    }

    /**
     * This method amounts to calling
     * <CODE>fires(from,to,guard,null,null);</CODE>.
     */
//    public void fires(AbstractStatechart from, AbstractStatechart to, boolean guard) throws Statechart_transition_based_exception {
//        fires(from, to, guard, null, null);
//    }
    /**
     * This method amounts to calling
     * <CODE>fires(event,from,to,guard,null,null);</CODE>.
     */
    public void fires(String event, AbstractStatechart from, AbstractStatechart to, boolean guard) throws Statechart_transition_based_exception {
        fires(event, from, to, guard, null, null);
    }

    /**
     * This method amounts to calling
     * <CODE>fires(from,to,guard,object,action,null);</CODE>.
     */
//    public void fires(AbstractStatechart from, AbstractStatechart to, boolean guard, Object object, String action) throws Statechart_transition_based_exception {
//        fires(from, to, guard, object, action, null);
//    }
    /**
     * This method amounts to calling
     * <CODE>fires(event,from,to,guard,object,action,null);</CODE>.
     */
    public void fires(String event, AbstractStatechart from, AbstractStatechart to, boolean guard, Object object, String action) throws Statechart_transition_based_exception {
        fires(event, from, to, guard, object, action, null);
    }

    /**
     * This method amounts to calling
     * <CODE>fires(from,to,guard,object,action,args,AbstractStatechart.No_reentrance);</CODE>.
     */
//    public void fires(AbstractStatechart from, AbstractStatechart to, boolean guard, Object object, String action, Object[] args) throws Statechart_transition_based_exception {
//        fires(from, to, guard, object, action, args, AbstractStatechart.No_reentrance);
//    }
    /**
     * This method amounts to calling
     * <CODE>fires(event,from,to,guard,object,action,args,AbstractStatechart.No_reentrance);</CODE>.
     */
    public void fires(String event, AbstractStatechart from, AbstractStatechart to, boolean guard, Object object, String action, Object[] args) throws Statechart_transition_based_exception {
        fires(event, from, to, guard, object, action, args, AbstractStatechart.No_reentrance);
    }

    /**
     * This method amounts to calling
     * <CODE>fires(from,to,null,"true",object,action,args,reentrance_mode);</CODE>
     * if <CODE>guard</CODE> is <CODE>true</CODE>,
     * <CODE>fires(from,to,null,"false",object,action,args,reentrance_mode);</CODE>
     * otherwise.
     */
//    public void fires(AbstractStatechart from, AbstractStatechart to, boolean guard, Object object, String action, Object[] args, byte reentrance_mode) throws Statechart_transition_based_exception {
//        if (guard) {
//            fires(from, to, null, "true", object, action, args, reentrance_mode);
//        } else {
//            fires(from, to, null, "false", object, action, args, reentrance_mode);
//        }
//    }
    /**
     * This method amounts to calling
     * <CODE>fires(event,from,to,null,"true",object,action,args,reentrance_mode);</CODE>
     * if <CODE>guard</CODE> is <CODE>true</CODE>,
     * <CODE>fires(event,from,to,null,"false",object,action,args,reentrance_mode);</CODE>
     * otherwise.
     */
    public void fires(String event, AbstractStatechart from, AbstractStatechart to, boolean guard, Object object, String action, Object[] args, byte reentrance_mode) throws Statechart_transition_based_exception {
        if (guard) {
            fires(event, from, to, null, "true", null, object, action, args, reentrance_mode);
        } else {
            fires(event, from, to, null, "false", null, object, action, args, reentrance_mode);
        }
    }

    /**
     * This method amounts to calling
     * <CODE>fires(from,to,guard_object,guard_action,null,null);</CODE>.
     */
//    public void fires(AbstractStatechart from, AbstractStatechart to, Object guard_object, String guard_action) throws Statechart_transition_based_exception {
//        fires(from, to, guard_object, guard_action, null, null);
//    }
    /**
     * This method amounts to calling
     * <CODE>fires(from,to,guard_object,guard_action,guard_args,null,null,null);</CODE>.
     */
//    public void fires(AbstractStatechart from, AbstractStatechart to, Object guard_object, String guard_action, Object[] guard_args) throws Statechart_transition_based_exception {
//        fires(from, to, guard_object, guard_action, guard_args, null, null, null);
//    }
    /**
     * This method amounts to calling
     * <CODE>fires(event,from,to,guard_object,guard_action,null,null);</CODE>.
     */
    public void fires(String event, AbstractStatechart from, AbstractStatechart to, Object guard_object, String guard_action) throws Statechart_transition_based_exception {
        fires(event, from, to, guard_object, guard_action, null, null);
    }

    /**
     * This method amounts to calling
     * <CODE>fires(event,from,to,guard_object,guard_action,guard_args,null,null,null);</CODE>.
     */
    public void fires(String event, AbstractStatechart from, AbstractStatechart to, Object guard_object, String guard_action, Object[] guard_args) throws Statechart_transition_based_exception {
        fires(event, from, to, guard_object, guard_action, guard_args, null, null, null);
    }

    /**
     * This method amounts to calling
     * <CODE>fires(from,to,guard_object,guard_action,object,action,null);</CODE>.
     */
//    public void fires(AbstractStatechart from, AbstractStatechart to, Object guard_object, String guard_action, Object object, String action) throws Statechart_transition_based_exception {
//        fires(from, to, guard_object, guard_action, object, action, null);
//    }
    /**
     * This method amounts to calling
     * <CODE>fires(from,to,guard_object,guard_action,guard_args,object,action,null);</CODE>.
     */
//    public void fires(AbstractStatechart from, AbstractStatechart to, Object guard_object, String guard_action, Object[] guard_args, Object object, String action) throws Statechart_transition_based_exception {
//        fires(from, to, guard_object, guard_action, guard_args, object, action, null);
//    }
    /**
     * This method amounts to calling
     * <CODE>fires(event,from,to,guard_object,guard_action,object,action,null);</CODE>.
     */
    public void fires(String event, AbstractStatechart from, AbstractStatechart to, Object guard_object, String guard_action, Object object, String action) throws Statechart_transition_based_exception {
        fires(event, from, to, guard_object, guard_action, object, action, null);
    }

    /**
     * This method amounts to calling
     * <CODE>fires(event,from,to,guard_object,guard_action,guard_args,object,action,null);</CODE>.
     */
    public void fires(String event, AbstractStatechart from, AbstractStatechart to, Object guard_object, String guard_action, Object[] guard_args, Object object, String action) throws Statechart_transition_based_exception {
        fires(event, from, to, guard_object, guard_action, guard_args, object, action, null);
    }

    /**
     * This method amounts to calling
     * <CODE>fires(from,to,guard_object,guard_action,object,action,args,AbstractStatechart.No_reentrance);</CODE>.
     */
//    public void fires(AbstractStatechart from, AbstractStatechart to, Object guard_object, String guard_action, Object object, String action, Object[] args) throws Statechart_transition_based_exception {
//        fires(from, to, guard_object, guard_action, object, action, args, AbstractStatechart.No_reentrance);
//    }
    /**
     * This method amounts to calling
     * <CODE>fires(from,to,guard_object,guard_action,guard_args,object,action,args,AbstractStatechart.No_reentrance);</CODE>.
     */
//    public void fires(AbstractStatechart from, AbstractStatechart to, Object guard_object, String guard_action, Object[] guard_args, Object object, String action, Object[] args) throws Statechart_transition_based_exception {
//        fires(from, to, guard_object, guard_action, guard_args, object, action, args, AbstractStatechart.No_reentrance);
//    }
    /**
     * This method amounts to calling
     * <CODE>fires(event,from,to,guard_object,guard_action,null,object,action,args,AbstractStatechart.No_reentrance);</CODE>.
     */
    public void fires(String event, AbstractStatechart from, AbstractStatechart to, Object guard_object, String guard_action, Object object, String action, Object[] args) throws Statechart_transition_based_exception {
        fires(event, from, to, guard_object, guard_action, null, object, action, args, AbstractStatechart.No_reentrance);
    }

    /**
     * This method amounts to calling
     * <CODE>fires(event,from,to,guard_object,guard_action,guard_args,object,action,args,AbstractStatechart.No_reentrance);</CODE>.
     */
    public void fires(String event, AbstractStatechart from, AbstractStatechart to, Object guard_object, String guard_action, Object[] guard_args, Object object, String action, Object[] args) throws Statechart_transition_based_exception {
        fires(event, from, to, guard_object, guard_action, guard_args, object, action, args, AbstractStatechart.No_reentrance);
    }

    /**
     * This method amounts to calling
     * <CODE>fires(Pseudo_event,from,to,guard_object,guard_action,null,object,action,args,reentrance_mode);</CODE>.
     *
     * @see AbstractStatechart#Pseudo_event
     */
//    public void fires(AbstractStatechart from, AbstractStatechart to, Object guard_object, String guard_action, Object object, String action, Object[] args, byte reentrance_mode) throws Statechart_transition_based_exception {
//        fires(Pseudo_event, from, to, guard_object, guard_action, null, object, action, args, reentrance_mode);
//    }
    /**
     * This method amounts to calling
     * <CODE>fires(Pseudo_event,from,to,guard_object,guard_action,guard_args,object,action,args,reentrance_mode);</CODE>.
     *
     * @see AbstractStatechart#Pseudo_event
     */
//    public void fires(AbstractStatechart from, AbstractStatechart to, Object guard_object, String guard_action, Object[] guard_args, Object object, String action, Object[] args, byte reentrance_mode) throws Statechart_transition_based_exception {
//        fires(Pseudo_event, from, to, guard_object, guard_action, guard_args, object, action, args, reentrance_mode);
//    }
    /**
     * This method creates and registers (caches) a transition labeled with an
     * event name in a state machine (see: <I>UML State Machine Diagrams</I>
     * formalism).
     *
     * @param event The name of the event labeling the transition
     * @param from The state being the origin of the transition
     * @param to The state being the end of the transition
     * @param guard_object The object in charge of executing the guard (if any)
     * @param guard_action The name of the action to be executed in order to
     * establish the guard's evaluation; This is required * * * * * * * * * * *
     * if <CODE>guard_object</CODE> is used
     * @param guard_args The arguments (if any) of the action which represents
     * the guard
     * @param object The object in charge of executing the action, if any,
     * triggered by the event; Multiple actions require multiple use of this
     * method
     * @param action The name of the action to be executed; This is required *
     * if <CODE>object</CODE> is used
     * @param args The arguments of the action to be executed * * * * * * * * *
     * or <CODE>null</CODE>
     * @param reentrance_mode A flag value which is equal to
     * {@link AbstractStatechart#Reentrance} or
     * {@link AbstractStatechart#No_reentrance}; This value enables or disables
     * the reentrance_mode mode of communication for the action to be executed
     */
    synchronized public void fires(String event, AbstractStatechart from, AbstractStatechart to, Object guard_object, String guard_action, Object[] guard_args, Object object, String action, Object[] args, byte reentrance_mode) throws Statechart_transition_based_exception {
        if (event == null || from == null || to == null) {
            return;
        }
        if (from.hasForSuperState(to)) {
            throw new Statechart_transition_based_exception("Incorrect transition from substate to superstate", from, to);
        }
        if (from.isAndWith(to)) {
            throw new Statechart_transition_based_exception("Incorrect transition between orthogonal states", from, to);
        }
        Transition transition = new Transition(from, to);
        AbstractGuard guard = guard(guard_object, guard_action, guard_args);
        Hashtable events = (Hashtable) _transitions.get(transition);
        Hashtable guards;
        Vector actions;
        if (events == null) {
            events = new Hashtable();
            guards = new Hashtable();
            actions = new Vector();
            guards.put(guard, actions);
            events.put(event, guards);
            _transitions.put(transition, events);
        } else {
            guards = (Hashtable) events.get(event);
            if (guards == null) {
                guards = new Hashtable();
                actions = new Vector();
                guards.put(guard, actions);
                events.put(event, guards);
            } else {
                actions = (Vector) guards.remove(guard);
                if (actions == null) {
                    actions = new Vector();
                }
                guards.put(guard, actions);
            }
        }
        if (object != null && action != null) {
            AbstractAction new_action = action(object, action, args, reentrance_mode);
            /**
             * Removed from ver. 1.3
             */
//            Boolean value = (Boolean) _runtime_events.get(event);
//            if (value != null) {
//                if (value == Boolean.TRUE) {
//                    actions.removeAllElements();
//                    _runtime_events.put(event, Boolean.FALSE);
//                }
//            }
            /**
             * End of removed from ver. 1.3
             */
            // The following statements rely on a specific implementation of the 'public boolean equals(Object action)' method in the 'AbstractAction' class:
            int index = actions.indexOf(new_action); // Search of first index is safe because duplicates have not been allowed
            if (index != -1 /* && _runtime_events.containsKey(event)*/) {
                actions.removeElementAt(index); // Arguments' update, e.g., 'push' event in 'My_stack' component
                actions.add(index, new_action);
            } else {
                actions.addElement(new_action);
            }
        }
    }

    /**
     * This method unregisters a transition labeled with an event name in a
     * state machine (see: <I>UML State Machine Diagrams</I>
     * formalism).
     *
     * @param event The name of the event labeling the transition
     * @param from The state being the origin of the transition
     * @param to The state being the end of the transition
     * @param guard Only simple guards are supported at this time
     * @param object The object in charge of executing the action, if any,
     * triggered by the event; Multiple actions require multiple use of this
     * method
     * @param action The name of the action to be executed; This is required *
     * if <CODE>object</CODE> is used
     * @param args The arguments of the action to be executed * * * * * * * * *
     * or <CODE>null</CODE>
     * @param reentrance_mode A flag value which is equal to
     * {@link AbstractStatechart#Reentrance} or
     * {@link AbstractStatechart#No_reentrance}; This value enables or disables
     * the reentrance_mode mode of communication for the action to be executed
     */
    synchronized public void unfires(String event, AbstractStatechart from, AbstractStatechart to, boolean guard, Object object, String action, Object[] args, byte reentrance_mode) throws Statechart_transition_based_exception {
        if (event == null || from == null || to == null) {
            return;
        }
        Transition t = new Transition(from, to);
        Hashtable events = (Hashtable) _transitions.get(t);
        if (events != null) {
            Hashtable guards = (Hashtable) events.get(event);
            if (guards != null) {
                AbstractGuard g = guard(null, guard ? "true" : "false", null);
                Vector actions = (Vector) guards.get(g);
                if (actions != null && object != null && action != null) {
                    AbstractAction a = action(object, action, args, reentrance_mode);
                    int index = actions.indexOf(a);
                    if (index != -1) {
                        actions.removeElementAt(index);
                        if (actions.isEmpty()) {
                            guards.remove(g);
                            if (guards.isEmpty()) {
                                _transitions.remove(t);
                            }
                        }
                    }
                } else {
                    guards.remove(g);
                    if (guards.isEmpty()) {
                        _transitions.remove(t);
                    }
                }
            }
        }
    }

    /**
     * This method amounts to calling
     * <CODE>run_to_completion(Pseudo_event);</CODE>.
     *
     * @see AbstractStatechart#Pseudo_event
     */
//    public void run_to_completion() throws Statechart_exception {
//        run_to_completion(Pseudo_event);
//    }
    /**
     * This method amounts to calling
     * <CODE>run_to_completion(event,AbstractStatechart_monitor.Don_t_compute_invariants);</CODE>
     */
    public void run_to_completion(String event) throws Statechart_exception {
        run_to_completion(event, AbstractStatechart_monitor.Don_t_compute_invariants);
    }

    /**
     * This method is the key mechanism of the <I>PauWare</I> software; It moves
     * a state machine from one stable consistent context to another.
     *
     * @param event The name of the event to be processed (<CODE>null</CODE>
     * means no effect); Only transitions labeled and registred with this event
     * name are processed (see: the
     * {@link AbstractStatechart_monitor#fires(String,AbstractStatechart,AbstractStatechart,Object,String,Object[],Object,String,Object[],byte)}
     * method)
     * @param compute_invariants This mode triggers the calculation of
     * invariants
     */
    synchronized public void run_to_completion(String event, boolean compute_invariants) throws Statechart_exception {
        /**
         * Lea
         */
        if(PauWareManager.isTraceable())
            PauWareManager.trace.startCompletionCycle(event);

        
        if (event == null) {
            return;
        }
        
        /**
         * Removed from ver. 1.3
         */
//        Boolean value = (Boolean) _runtime_events.get(event);
//        if (value == null || (value != null && value == Boolean.FALSE)) {
//            _runtime_events.put(event, Boolean.TRUE);
//        }
        /**
         * End of removed from ver. 1.3
         */
        _eliminated_transitions.removeAllElements();
        _execution.clear();
        _verbose.delete(0, _verbose.length());
        _verbose.append(_name + '.' + event + "\n");
        
        // review of all transitions:
        for (Enumeration transitions = _transitions.keys(); transitions.hasMoreElements();) {
            Transition transition = (Transition) transitions.nextElement();
            AbstractStatechart from = transition._from;
            AbstractStatechart to = transition._to;
            Hashtable events = (Hashtable) _transitions.get(transition);
            Hashtable cached_guards = (Hashtable) events.get(event);
            Hashtable runtime_guards = (Hashtable) events.get(Pseudo_event);
            if (cached_guards == null) {
                if (runtime_guards != null) {
                    cached_guards = new Hashtable();
                    for (Enumeration elements = runtime_guards.keys(); elements.hasMoreElements();) {
                        AbstractGuard runtime_guard = (AbstractGuard) elements.nextElement();
                        cached_guards.put(runtime_guard, runtime_guards.get(runtime_guard));
                    }
                    events.put(event, cached_guards);
                    events.remove(Pseudo_event);
                }
            } else {
                if (runtime_guards != null) {
                    if (cached_guards != runtime_guards) { // i.e., ! event.equals(Pseudo_event)
                        for (Enumeration elements = runtime_guards.keys(); elements.hasMoreElements();) {
                            AbstractGuard runtime_guard = (AbstractGuard) elements.nextElement();
                            Vector runtime_actions = (Vector) runtime_guards.get(runtime_guard);
                            Vector cached_actions = (Vector) cached_guards.remove(runtime_guard);
                            for (int i = 0; cached_actions != null && i < cached_actions.size(); i++) {
                                AbstractAction cached_action = (AbstractAction) cached_actions.elementAt(i);
                                if (!runtime_actions.contains(cached_action)) {
                                    runtime_actions.addElement(cached_action);
                                }
                            }
                            cached_guards.put(runtime_guard, runtime_actions);
                        }
                    } else {
                        cached_guards = new Hashtable();
                        for (Enumeration elements = runtime_guards.keys(); elements.hasMoreElements();) {
                            AbstractGuard runtime_guard = (AbstractGuard) elements.nextElement();
                            cached_guards.put(runtime_guard, runtime_guards.get(runtime_guard));
                        }
                    }
                    // events.put(event,cached_guards); unuseful since 'event.equals(Pseudo_event) == true'
                    events.remove(Pseudo_event);
                }
            }

            if (cached_guards != null) {
                Hashtable new_cached_guards = new Hashtable();
                for (Enumeration elements = cached_guards.keys(); elements.hasMoreElements();) {
                    AbstractGuard guard = (AbstractGuard) elements.nextElement();
                    try {
                        if (guard.execute()) {
                            /**
                             * Lea
                             */
                            if(guard._action != null) {
                                if(PauWareManager.isTraceable()) {
                                   PauWareManager.trace.onGuardValid(transition,guard);
                                }
                            }
                            
                            Vector executable_actions = (Vector) cached_guards.get(guard);
                            if (from._active) {
                                if (_execution.containsKey(transition)) {
                                    throw new Statechart_transition_based_exception("Found at least two conflicting eligible transitions with same source and same target states\nSecond transition creating the conflict", from, to);
                                } else {
                                    _verbose.append("\tELIGIBLE TRANSITION: " + from._name + " -> " + to._name + ", guard: [" + guard.verbose() + "]\n");
                                    /**
                                     * Lea
                                     */
                                    if(PauWareManager.isTraceable())
                                        PauWareManager.trace.onFiredTransition(transition);
                                   
                                    // actions associated with 'guard.execute() == true' are going to be executed:
                                    Object[] executed_actions = new Object[executable_actions.size()];
                                    executable_actions.copyInto(executed_actions);
                                    _execution.put(transition, executed_actions);
                                }
                            }
                            // runtime guards whose value is 'true' are going to be incorporated into the cache with value 'false':
                            if (runtime_guards != null && runtime_guards.containsKey(guard)) {
                                guard._value = false;
                                Vector cached_actions = (Vector) cached_guards.get(guard);
                                if (cached_actions != null) {
                                    if (cached_actions != executable_actions) {
                                        for (int i = 0; i < cached_actions.size(); i++) {
                                            AbstractAction cached_action = (AbstractAction) cached_actions.elementAt(i);
                                            if (!executable_actions.contains(cached_action)) {
                                                executable_actions.addElement(cached_action);
                                            }
                                        }
                                    }
                                    new_cached_guards.put(guard, executable_actions);
                                } else {
                                    if (executable_actions != null) {
                                        new_cached_guards.put(guard, executable_actions);
                                    } else {
                                        new_cached_guards.put(guard, new Vector());
                                    }
                                }
                            } else {
                                new_cached_guards.put(guard, executable_actions);
                            }
                        } else {
                            
                            /**
                             * Lea
                             */
                            if(PauWareManager.isTraceable())
                                PauWareManager.trace.onGuardUnvalid(transition, guard);
                            
                            // 'guard.execute() == false'
                            if (!new_cached_guards.containsKey(guard)) {
                                new_cached_guards.put(guard, cached_guards.get(guard));
                            }
                        }
                    } catch (Statechart_exception se) {
                        _verbose.append("\t{WARNING}canceled: " + from._name + " -> " + to._name + ", guard: [" + guard.verbose() + "]\n");
                    }
                }
                events.put(event, new_cached_guards);
            }
        }

        // eliminated transitions resulting from event overriding:
        Enumeration eligible_transitions = _execution.keys();
        for (int i = 0; i < _execution.size() - 1; i++) {
            Transition transition = (Transition) eligible_transitions.nextElement();
            AbstractStatechart from = transition._from;
            AbstractStatechart to = transition._to;
            Enumeration other_eligible_transitions = _execution.keys();
            for (int j = 0; j <= i; j++) {
                other_eligible_transitions.nextElement();
            }
            for (int j = i + 1; j < _execution.size(); j++) {
                Transition other_transition = (Transition) other_eligible_transitions.nextElement();
                AbstractStatechart other_from = other_transition._from;
                AbstractStatechart other_to = other_transition._to;
                if (from == other_from) {
                    if (to != other_to && !to.isAndWith(other_to) || (!to.hasForSuperState(from) || !other_to.hasForSuperState(from))) {
                        throw new Statechart_transition_based_exception("Found at least two conflicting triggerable transitions with two incompatible target states\nSecond transition creating the conflict", other_from, other_to);
                    }
                } else {
                    if (from.hasForSuperState(other_from) && !_eliminated_transitions.contains(other_transition) && !to.isAndWith(other_to)) {
                        
                        _verbose.append("\t{WARNING}overridden: " + other_from._name + " -> " + other_to._name + ", guard: [true]\n");
                        _eliminated_transitions.addElement(other_transition);
                    }
                    if (other_from.hasForSuperState(from) && !_eliminated_transitions.contains(transition) && !other_to.isAndWith(to)) {
                        _verbose.append("\t{WARNING}overridden: " + from._name + " -> " + to._name + ", guard: [true]\n");
                        _eliminated_transitions.addElement(transition);
                    }
                }
            }
        }
        // start of execution, execution of allowed events:
        allowedEvent(event, _execution, _eliminated_transitions, _verbose);
        for (int i = 0; i < _eliminated_transitions.size(); i++) {
            _execution.remove(_eliminated_transitions.elementAt(i));
        }
        // execution of 'exit' actions:
        try {
            for (Enumeration executable_transitions = _execution.keys(); executable_transitions.hasMoreElements();) {
                Transition transition = (Transition) executable_transitions.nextElement();
                AbstractStatechart from = transition._from;
                AbstractStatechart to = transition._to;
                disactivate(from, to, _verbose);
            }
        } catch (Statechart_exception se) {
            /**
             * Lea
             */
            if(PauWareManager.isTraceable())
                PauWareManager.trace.onError(se);
            _verbose.append("\t\t{SEVERE} " + se.getMessage() + "\n");
        }

        // execution of actions associated with events:
        for (Enumeration executable_transitions = _execution.keys(); executable_transitions.hasMoreElements();) {
            /**
             * Lea
             */
            Object current_transition = executable_transitions.nextElement();
            //Object[] executed_actions = (Object[]) _execution.get(executable_transitions.nextElement());
            Object[] executed_actions = (Object[]) _execution.get(current_transition);
            
            for (int i = 0; i < executed_actions.length; i++) {
                AbstractAction executed_action = (AbstractAction) executed_actions[i];
                try {
                    executed_action.execute();
                    _verbose.append("\t\tEXECUTED ACTION: ");
                } catch (Statechart_exception se) {
                    _verbose.append("\t\t{WARNING}action failed: " + "\n");
                } finally {
                    _verbose.append(executed_action.verbose() + "\n");
                    /**
                     * Lea
                     */
                    if(PauWareManager.isTraceable())
                        PauWareManager.trace.onTransitionOperation((Transition) current_transition, executed_action);
                }
            }
        }

        // execution of 'entry' actions followed by 'do' activities:
        try {
            for (Enumeration executable_transitions = _execution.keys(); executable_transitions.hasMoreElements();) {
                Transition transition = (Transition) executable_transitions.nextElement();
                AbstractStatechart from = transition._from;
                AbstractStatechart to = transition._to;
                activate(from, to, _verbose);
            }
        } catch (Statechart_exception se) {
            _verbose.append("\t\t{SEVERE}" + se.getMessage() + "\n");
        }

        // Finalization:
        _current_state = current_state();
        _verbose.append("\tCURRENT STATE: " + _current_state);

        // Evaluation of invariants:
        if (compute_invariants) {
            try {
                _verbose.append(deepStateInvariant());
            } catch (Statechart_exception se) {
                
                /**
                 * Lea
                 */
                if(PauWareManager.isTraceable())
                   PauWareManager.trace.onError(se);
                
                _verbose.append(se.getMessage());
            } finally {
                _verbose.append("\n");
            }
        }
        try {
            if (_listener != null) {
                _listener.run_to_completion(_verbose.toString(), _execution);
            }
        } catch (Exception e) {
            throw new Statechart_exception(e.getMessage());
        }
        if (_show_on_system_out) {
            System.out.println(_verbose);
        }
        /**
         * Lea
         */
         if(PauWareManager.isTraceable())
            PauWareManager.trace.endCompletionCycle(event);

    }

    /**
     * This method launches a state machine.
     */
    synchronized public void start() throws Statechart_exception {
        try {
            if (_listener != null) {
                _listener.post_construct(this);
            }
            _verbose.delete(0, _verbose.length());
            _verbose.append("***START***\n");
            
            /**
             * Lea
             */
            if(PauWareManager.isTraceable())
            	PauWareManager.trace.onStateMachineStart(this);
            
            entry(null, _verbose);
            if (_show_on_system_out) {
                System.out.println(_verbose);
            }
            if (_listener != null) {
                _listener.start(_verbose.toString());
            }
        } catch (Exception e) {
            /**
                 * Lea
                 */
                if(PauWareManager.isTraceable())
                    PauWareManager.trace.onError(new Statechart_exception(e.getMessage()));
            throw new Statechart_exception(e.getMessage());
        }
    }

    /**
     * This method stops a state machine.
     */
    synchronized public void stop() throws Statechart_exception {
        try {
            _verbose.delete(0, _verbose.length());
            _verbose.append("***STOP***\n");
            exit(_verbose);
            if (_show_on_system_out) {
                System.out.println(_verbose);
            }
            if (_listener != null) {
                _listener.stop(_verbose.toString());
                _listener.pre_destroy();
            }
            
            /**
             * Lea
             */
            if(PauWareManager.isTraceable())
            	PauWareManager.trace.onStateMachineStop(this);
            
            
        } catch (Exception e) {
            throw new Statechart_exception(e.getMessage());
        }
    }

    /**
     * Management facility: this method is offered by a software component,
     * which implements the
     * {@link com.pauware.pauware_engine._Core.Manageable_base} interface.
     */
    public String async_current_state() {
        return _current_state;
    }

    /**
     * Management facility: this method is offered by a software component,
     * which implements the
     * {@link com.pauware.pauware_engine._Core.Manageable_base} interface.
     */
    synchronized public void to_state(String name) throws Statechart_exception {
        AbstractStatechart to = lookup(name);
        if (to != null) {
            try {
                _verbose.delete(0, _verbose.length());
                _verbose.append("***TO STATE: " + name + "***\n");
                disactivate(this, to, _verbose);
                if (_show_on_system_out) {
                    System.out.println(_verbose);
                }
                if (_listener != null) {
                    _listener.stop(_verbose.toString());
                }
                _verbose.delete(0, _verbose.length());
                _verbose.append("***RESTART FROM: " + name + "***\n");
                activate(this, to, _verbose);
                if (_show_on_system_out) {
                    System.out.println(_verbose);
                }
                if (_listener != null) {
                    _listener.start(_verbose.toString());
                }
            } catch (Exception e) {
                throw new Statechart_exception(e.getMessage());
            }
        }
    }

    /**
     * Management facility: this method is offered by a software component,
     * which implements the
     * {@link com.pauware.pauware_engine._Core.Manageable_base} interface.
     */
    public String verbose() {
        return _verbose.toString();
    }

    /**
     * This method is used when an {@link AbstractStatechart_monitor} is put in
     * a hashtable as a key.
     */
    public boolean equals(Object s) {
        if (this == s) {
            return true;
        }
        if (s instanceof AbstractStatechart_monitor) {
            return _name.equals(((AbstractStatechart_monitor) s)._name);
        }
        return false;
    }

    /**
     * This method is used when an {@link AbstractStatechart_monitor} is put in
     * a hashtable as a key.
     */
    public int hashCode() {
        return _name.hashCode();
    }

    /**
     * For visualization purposes only: this method may be used by the
     * <I>PauWare view</I>
     * tool.
     */
    public Hashtable transitions() {
        return _transitions;
    }

    /**
     * For visualization purposes only: this method may be used by the
     * <I>PauWare view</I>
     * tool.
     */
    public String to_UML(String event, Transition transition, AbstractGuard guard) {
        if (event == null || transition == null || guard == null) {
            return "";
        }
        StringBuffer label = new StringBuffer("");
        Hashtable events = (Hashtable) _transitions.get(transition);
        if (events != null) {
            Hashtable guards = (Hashtable) events.get(event);
            if (guards != null) {
                label.append(event);
                Vector actions = (Vector) guards.get(guard);
                if (actions != null) {
                    String guard_to_UML = guard.to_UML();
                    if (guard_to_UML != null) {
                        label.append("[" + guard_to_UML + "]");
                    }
                    label.append("/");
                    Enumeration e = actions.elements();
                    while (e.hasMoreElements()) {
                        String action_to_UML = ((AbstractAction) e.nextElement()).to_UML();
                        if (action_to_UML != null) {
                            label.append(action_to_UML + ",");
                        }
                    }
                    label.deleteCharAt(label.length() - 1);
                }
            }
        }
        return label.toString();
    }

    /**
     * For visualization purposes only: this method is used by the
     * <I>PauWare view</I> tool to compute the state machine's visualization
     * graph. Since ver. 1.3.
     */
    public String to_PlantUML() {
        return compute_state_views(compute_transition_views(new Hashtable()));
    }

    /**
     * For visualization purposes only: this method is internally called by the
     * {@link AbstractStatechart_monitor#to_PlantUML()} method to compute the
     * transition views of the state machine. Since ver. 1.3.
     */
    private Hashtable compute_transition_views(Hashtable transition_views) {
        for (Enumeration transition_elements = _transitions.keys(); transition_elements.hasMoreElements();) {
            Transition transition = (Transition) transition_elements.nextElement();
            AbstractStatechart from = transition.from();
            AbstractStatechart to = transition.to();
            Hashtable events = (Hashtable) _transitions.get(transition);
            String transition_view = "";
            for (Enumeration event_elements = events.keys(); event_elements.hasMoreElements();) {
                String event = (String) event_elements.nextElement();
                transition_view += Clean_up(from.name()) + " --> " + Clean_up(to.name()) + ':' + Textual_view_subject_separator + Clean_up(event);
                Hashtable guarded_actions = (Hashtable) events.get(event);
                for (Enumeration guarded_action_elements = guarded_actions.keys(); guarded_action_elements.hasMoreElements();) {
                    AbstractGuard guard = (AbstractGuard) guarded_action_elements.nextElement();
                    transition_view += guard.to_UML();
                    if (!((Vector) guarded_actions.get(guard)).isEmpty()) {
                        transition_view += '/';
                    }
                    for (Enumeration action_elements = ((Vector) guarded_actions.get(guard)).elements(); action_elements.hasMoreElements();) {
                        AbstractAction action = (AbstractAction) action_elements.nextElement();
                        transition_view += action.to_UML() + String.valueOf(Sequence_character);
                    }
                    if (transition_view.charAt(transition_view.length() - 1) == Sequence_character) {
                        transition_view = transition_view.substring(0, transition_view.lastIndexOf(Sequence_character));
                    }
                }
                transition_view += Textual_view_subject_separator + transition.hashCode() + Textual_view_subject_separator + "\n";
            }
            AbstractStatechart commonSuper = to.commonSuperWith(from);
            if (transition_views.get(commonSuper) == null) {
                transition_views.put(commonSuper, transition_view);
            } else {
                transition_views.put(commonSuper, ((String) transition_views.get(commonSuper)) + transition_view);
            }
        }
        return transition_views;
    }
}
