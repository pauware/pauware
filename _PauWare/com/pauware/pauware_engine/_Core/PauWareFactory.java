/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pauware.pauware_engine._Core;

/**
 * The PauWare factory helps in implementing PauWare programs independtly of the underlying Java platform.
 * It return a PauWare manager based on a platform choice.
 * @author ercar
 */
public class PauWareFactory {
        
    /**
     * The PauWare manager for the Java EE platform
     */
    public static IPauWare pauwareManagerEE = null;
    
    /**
     * The PauWare manager for the Java ME platform
     */
    public static IPauWare pauwareManagerME = null;
    
    /**
     * Return the PauWare manager for a given Java platform
     * @param platform the Java platform
     * @return the concrete PauWare manager for this platform
     */
    public static IPauWare getPauWareManager(JavaPlatform platform) {
        if (platform.equals(JavaPlatform.JavaEE)) {
            if (pauwareManagerEE == null) 
                pauwareManagerEE = new PauWareManagerEE();
            return pauwareManagerEE;
        }
        if (platform.equals(JavaPlatform.JavaME)) {
            if (pauwareManagerME == null) 
                pauwareManagerME = new PauWareManagerME();
            return pauwareManagerME;
        }
        
        return null;
    } 
}
