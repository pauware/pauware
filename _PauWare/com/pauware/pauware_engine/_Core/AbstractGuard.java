/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Core;

import com.pauware.pauware_engine._Exception.Statechart_exception;

/**
 * This abstract class represents the general notion of <I>Guard</I> in UML.
 * Instances of this class are executed at event processing time (notation is:
 * <I>event[guard]/action(s)</I>). . Use of this class occurs through the
 * following subclasses: {@link com.pauware.pauware_engine._Java_EE.Guard}
 * (<I>PauWare Java EE</I> - Java SE/Java EE) or
 * {@link com.pauware.pauware_engine._Java_ME.Java_MEGuard} (<I>PauWare Java
 * ME</I> - Java SE/Java ME). It is instantiated within the
 * {@link com.pauware.pauware_engine._Core.AbstractStatechart_monitor#fires(String,AbstractStatechart,AbstractStatechart,Object,String,Object[],Object,String,Object[],byte)}
 * method. Instances of this class are executed by means of the
 * {@link com.pauware.pauware_engine._Core.AbstractGuard#execute()} method
 * during a run-to-completion cycle
 * ({@link com.pauware.pauware_engine._Core.AbstractStatechart_monitor#run_to_completion(String,boolean)}
 * method).
 * <p>
 * Compatibility: <I>PauWare Java EE</I> (Java SE/Java EE) and
 * <I>PauWare Java ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
abstract public class AbstractGuard {

    /**
     * The action to be evaluated. This field is <CODE>null</CODE> when no
     * action is associated with a guard. In such a case, the
     * {@link AbstractGuard#_value} field is computed at the beginning of a
     * run-to-completion cycle and frozen during this cycle.
     */
    protected AbstractAction _action = null;
    /**
     * The result of the guard's evaluation. <b>Caution</b>: guards raising
     * exceptions or errors during their evaluation are set to
     * <b><CODE>false</CODE></b>.
     */
    protected boolean _value = false;

    /**
     * This constructor is called by the
     * {@link AbstractStatechart_monitor#fires(String,AbstractStatechart,AbstractStatechart,Object,String,Object[],Object,String,Object[],byte)}
     * method.
     *
     * @param guard_object The object in charge of evaluating the guard; Most of
     * the time, this object is the software component that runs the state
     * machine.
     * @param guard_action The action to be executed; The execution amounts to
     * evaluating the guard.
     * @param guard_args The action's arguments.
     */
    protected AbstractGuard(Object guard_object, String guard_action, Object[] guard_args) {
        if (guard_object != null) {
            if (guard_action != null) {
                _action = action(guard_object, guard_action, guard_args);
            }
        } else {
            if (guard_action != null && guard_action.equals("true")) {
                _value = true;
            }
        }
    }

    /**
     * Implementation of this method depends upon the chosen platform.
     *
     * @see com.pauware.pauware_engine._Java_EE.Guard
     * @see com.pauware.pauware_engine._Java_ME.Java_MEGuard
     * @see com.pauware.pauware_engine._Java_EE.Action
     * @see com.pauware.pauware_engine._Java_ME.Java_MEAction
     */
    abstract protected AbstractAction action(Object guard_object, String guard_action, Object[] guard_args);

    /**
     * This method is used when a <I>Guard</I> is put in a hashtable as a key.
     */
    public boolean equals(Object guard) {
        if (this == guard) {
            return true;
        }
        if (guard instanceof AbstractGuard) {
            return _action == null ? true : _action.equals(((AbstractGuard) guard)._action) && _value == ((AbstractGuard) guard)._value;
        }
        return false;
    }

    /**
     * This method is used when a <I>Guard</I> is put in a hashtable as a key.
     */
    public int hashCode() {
        return (_action == null ? 0 : _action.hashCode()) ^ (_value ? 1 : 0);
    }

    /**
     * Implementation of this method depends upon the chosen platform.
     *
     * @throws Statechart_exception An encapsulation of any Java problem
     * resulting from evaluating the guard.
     */
    protected boolean execute() throws Statechart_exception {
        if (_action == null) {
            return _value;
        }
        _action.execute();
        return _action._result instanceof Boolean ? ((Boolean) _action._result).booleanValue() : false;
    }

    /**
     * This method returns the detailed result of the guard's execution. It
     * calls the {@link AbstractAction#verbose()} method if an action is
     * associated with the guard. Othrwise, it returns either
     * <CODE>"false"</CODE> or <CODE>"true"</CODE>.
     */
    protected String verbose() {
        if (_action == null) {
            if (_value == true) {
                return "true";
            }
            if (_value == false) {
                return "false";
            }
        }
        return _action.verbose();
    }

    /**
     * This method returns the guard in the form of a UML-compliant string.
     */
    public String to_UML() {
        return _action != null ? "[" + _action.to_UML() + "]" : "";
    }

    public AbstractAction getAction() {
        return _action;
    }

    public boolean isValue() {
        return _value;
    }
    
    
}
 