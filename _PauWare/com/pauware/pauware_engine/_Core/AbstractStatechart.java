/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Core;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Vector;

import com.pauware.pauware_engine._Exception.*;

/**
 * This abstract class represents the general notion of <I>State</I> in UML. Use
 * of this class occurs through the following subclasses:
 * {@link com.pauware.pauware_engine._Java_EE.Statechart} (<I>PauWare Java
 * EE</I> - Java SE/Java EE) or
 * {@link com.pauware.pauware_engine._Java_ME.Java_MEStatechart} (<I>PauWare
 * Java ME</I> - Java SE/Java ME). Since <I>UML State Machine Diagrams</I> are
 * nested, an instance of this class may be a leaf state or a state containing
 * substates.
 * <p>
 * Compatibility: <I>PauWare Java EE</I> (Java SE/Java EE) and
 * <I>PauWare Java ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
abstract public class AbstractStatechart {

    /**
     * This class variable defines the sign for <I>computing sequence</I>. Since
     * ver. 1.3.
     */
    public static final char Sequence_character = ';';

    /**
     * This class variable is a constant value which is used for the naming of
     * states when state machines are composed. Since ver. 1.2 (vertical
     * composition).
     */
    public static final String State_name_separator = "::";
    /**
     * This class variable is a constant value. It represents the fact that an
     * event sending IS NOT the cause of reentrance. This is the default mode in
     * PauWare.
     *
     * @see
     * com.pauware.pauware_engine._Core.AbstractStatechart_monitor#fires(String,AbstractStatechart,AbstractStatechart,Object,String,Object[],Object,String,Object[],byte)
     */
    public static final byte No_reentrance = -128;
    /**
     * This class variable is a constant value. It represents the fact that an
     * event sending IS the cause of reentrance. For an event sender (state
     * machine), using this constant value avoids receiving destabilizing events
     * while it has not completed its run-to-completion cycle.
     *
     * @see
     * com.pauware.pauware_engine._Core.AbstractStatechart_monitor#fires(String,AbstractStatechart,AbstractStatechart,Object,String,Object[],Object,String,Object[],byte)
     */
    public static final byte Reentrance = -127;
    /**
     * This class variable is a constant value (<CODE>"completion"</CODE>),
     * which is used for transitions that do not have a labeling event. As a
     * result, events <b>must not</b> be named <CODE>"completion"</CODE>.
     */
    public static final String Completion = "completion";
    /**
     * This class variable is a constant value (<CODE>"pseudo-event"</CODE>),
     * which is used to name events by default. As a result, events <b>must
     * not</b> be named <CODE>"pseudo-event"</CODE>.
     */
    public static final String Pseudo_event = "pseudo-event";
    /**
     * This class variable is a constant value ( <CODE>"pseudo-state"</CODE>)
     * which is used to name states by default. As a result, states <b>must
     * not</b> be named <CODE>"pseudo-state"</CODE>. Note: fictitious states are
     * created when states are composed together by means of the <I>nesting</I>,
     * <I>xor</I>
     * and <I>and</I> operators. If they are not named, these states have no
     * "true existence" in <I>UML State Machine Diagrams</I>. They consequently
     * just play the role of "composites" in order to properly organize the
     * state machine within the memory.
     *
     * @see AbstractStatechart#name(String)
     */
    public static final String Pseudo_state = "pseudo-state";
    /**
     * This field records if a state is active.
     *
     * @see AbstractStatechart#active()
     */
    protected boolean _active = false;
    /**
     * This field records the activity, if any, associated with a state.
     *
     * @see AbstractStatechart#doActivity(Object,String,Object[])
     */
    protected AbstractAction _do = null;
    /**
     * This field records the action(s), if any, associated with a state when
     * entering it.
     *
     * @see AbstractStatechart#set_entryAction(Object,String,Object[],byte)
     * @see AbstractStatechart#reset_entryAction(Object,String,Object[],byte)
     */
    protected final Vector _entry = new Vector();
    /**
     * This field records the action(s), if any, associated with a state when
     * exiting it.
     *
     * @see AbstractStatechart#set_exitAction(Object,String,Object[],byte)
     * @see AbstractStatechart#reset_exitAction(Object,String,Object[],byte)
     */
    protected final Vector _exit = new Vector();
    /**
     * This field records the allowed events of a state with their possible
     * associated guards and their respective associated actions. Note: by
     * definition, allowed events <b>bypass</b> <I>entry</I> and <I>exit</I>
     * actions.
     *
     * @see
     * AbstractStatechart#allowedEvent(String,Object,String,Object[],Object,String,Object[],byte)
     */
    protected final Hashtable _allowed_events = new Hashtable();
    /**
     * Removed from ver. 1.3
     */
    /**
     * This field records the events already processed during a
     * run-to-completion cycle. The key is the name of an event and its
     * associated value is a Boolean value which indicates if the event's
     * processing is in progress (<I>true</I>) or not (<I>false</I>).
     */
//    protected final Hashtable _runtime_events = new Hashtable();
    /**
     * End of removed from ver. 1.3
     */
    /**
     * This field records the invariant, if any, associated with a state.
     *
     * @see AbstractStatechart#stateInvariant(Object,String,Object[])
     */
    protected AbstractAction _invariant = null;
    /**
     * This field records if a state is an input state.
     *
     * @see AbstractStatechart#inputState()
     */
    protected boolean _inputState = false;
    /**
     * This field records if a state is an output state.
     *
     * @see AbstractStatechart#outputState()
     */
    protected boolean _outputState = false;
    /**
     * This field records the name of a state. The default name is
     * <CODE>Pseudo_state</CODE>.
     *
     * @see AbstractStatechart#Pseudo_state
     */
    protected String _name = Pseudo_state;
    /**
     * This field organizes a state machine as a binary tree. This field may be
     * left <CODE>null</CODE> (meaning that the state is a leaf state). In such
     * a case, one must have <CODE>_right == null</CODE>.
     */
    protected AbstractStatechart _left = null;
    /**
     * This field organizes a state machine as a binary tree. If the type of the
     * state owning this field is <CODE>AbstractStatechart</CODE>, this field
     * cannot be left <CODE>null</CODE> (meaning that the state is a direct or
     * indirect substate of the state machine itself). Otherwise if this state
     * is an instance of <CODE>AbstractStatechart_monitor</CODE>, this field
     * must be equal to <CODE>null</CODE>.
     */
    protected AbstractStatechart _node = null;
    /**
     * This field organizes a state machine as a binary tree. This field may be
     * left <CODE>null</CODE> (meaning that the state is a leaf state). In such
     * a case, one must have <CODE>_left == null</CODE>.
     */
    protected AbstractStatechart _right = null;
    /**
     * This field records if the two direct substates of this state are
     * exclusive or orthogonal.
     *
     * @see AbstractStatechart#xor(AbstractStatechart)
     * @see AbstractStatechart#and(AbstractStatechart)
     */
    protected boolean _xor = false;

    /**
     * Internal use only; This constructor creates a state. This constructor is
     * imposed by the fact that <CODE>Statechart</CODE> objects are
     * serializable.
     *
     * @see com.pauware.pauware_engine._Java_EE.Statechart
     */
    protected AbstractStatechart() {
    }

    /**
     * Internal use only; This constructor creates a state.
     *
     * @param name If name is equal to <CODE>null</CODE>, the name of the state
     * is set to <CODE>Pseudo_state</CODE>.
     *
     * @see AbstractStatechart#Pseudo_state
     */
    protected AbstractStatechart(String name) {
        if (name != null) {
            _name = name;
        }
    }

    /**
     * Implementation of this method depends upon the chosen platform.
     *
     * @see com.pauware.pauware_engine._Java_EE.Statechart
     * @see com.pauware.pauware_engine._Java_ME.Java_MEStatechart
     * @see com.pauware.pauware_engine._Java_EE.Action
     * @see com.pauware.pauware_engine._Java_ME.Java_MEAction
     */
    abstract protected AbstractAction action(Object object, String action, Object[] args, byte reentrance_mode);

    /**
     * Implementation of this method depends upon the chosen platform.
     *
     * @see com.pauware.pauware_engine._Java_EE.Statechart
     * @see com.pauware.pauware_engine._Java_ME.Java_MEStatechart
     * @see com.pauware.pauware_engine._Java_EE.Action
     * @see com.pauware.pauware_engine._Java_ME.Java_MEAction
     */
    abstract protected AbstractAction activity(Object object, String action, Object[] args);

    /**
     * Implementation of this method depends upon the chosen platform.
     *
     * @see com.pauware.pauware_engine._Java_EE.Statechart
     * @see com.pauware.pauware_engine._Java_ME.Java_MEStatechart
     * @see com.pauware.pauware_engine._Java_EE.Guard
     * @see com.pauware.pauware_engine._Java_ME.Java_MEGuard
     */
    abstract protected AbstractGuard guard(Object guard_object, String guard_action, Object[] guard_args);

    /**
     * Implementation of this method depends upon the chosen platform.
     *
     * @see com.pauware.pauware_engine._Java_EE.Statechart
     * @see com.pauware.pauware_engine._Java_ME.Java_MEStatechart
     */
    abstract protected AbstractStatechart createFather();

    /**
     * This method makes two states exclusive. States which are already linked
     * to other states (by nesting, exclusion or orthogonality relationships)
     * cannot be involved in this method. <I>PauWare</I> propagates nesting,
     * exclusion and orthogonality relationships through states by means of
     * transitivity.
     *
     * @see AbstractStatechart#hasForSuperState(AbstractStatechart)
     * @see AbstractStatechart#isAndWith(AbstractStatechart)
     * @see AbstractStatechart#isXorWith(AbstractStatechart)
     * @return A new state being the direct superstate of the two XOR-based
     * linked states. This new state may be named by using the
     * {@link AbstractStatechart#name(String)} method; Otherwise it is a
     * pseudo-state (see {@link AbstractStatechart#Pseudo_state})
     * @throws Statechart_state_based_exception A XOR error (concerning the two
     * incriminated states) if the exclusion link cannot be operated.
     */
    public AbstractStatechart xor(AbstractStatechart s) throws Statechart_state_based_exception {
        if (s == null || s._node != null || this._node != null) {
            throw new Statechart_state_based_exception("XOR error", this, s);
        }
        AbstractStatechart node = createFather();
        if (this._name.hashCode() < s._name.hashCode()) {
            node._left = this;
            node._right = s;
        } else {
            node._left = s;
            node._right = this;
        }
        this._node = node;
        s._node = node;
        node._xor = true;
        if (this._name.equals(Pseudo_state) && this._xor) {
            this._inputState = this._left._inputState | this._right._inputState;
        }
        if (s._name.equals(Pseudo_state) && s._xor) {
            s._inputState = s._left._inputState | s._right._inputState;
        }
        return node;
    }

    /**
     * This method makes the two argument states orthogonal. States which are
     * already linked to other states (by nesting, exclusion or orthogonality
     * relationships) cannot be involved in this method. <I>PauWare</I>
     * propagates nesting, exclusion and orthogonality relationships through
     * states by means of transitivity.
     *
     * @see AbstractStatechart#hasForSuperState(AbstractStatechart)
     * @see AbstractStatechart#isAndWith(AbstractStatechart)
     * @see AbstractStatechart#isXorWith(AbstractStatechart)
     * @return A new state being the direct superstate of the two AND-based
     * linked states. This new state may be named by using the
     * {@link AbstractStatechart#name(String)} method; Otherwise it is a
     * pseudo-state (see {@link AbstractStatechart#Pseudo_state})
     * @throws Statechart_state_based_exception An AND error with the two
     * incriminated states if the orthogonality link cannot be operated
     */
    public AbstractStatechart and(AbstractStatechart s) throws Statechart_state_based_exception {
        if (s == null || s._node != null || this._node != null) {
            throw new Statechart_state_based_exception("AND error", this, s);
        }
        AbstractStatechart node = createFather();
        if (this._name.hashCode() < s._name.hashCode()) {
            node._left = this;
            node._right = s;
        } else {
            node._left = s;
            node._right = this;
        }
        this._node = node;
        s._node = node;
        node._xor = false;
        return node;
    }

    /**
     * This method makes a state and a state machine orthogonal. Since ver. 1.2
     * (vertical composition).
     */
    public AbstractStatechart_monitor and(AbstractStatechart_monitor s) throws Statechart_state_based_exception {
        return (AbstractStatechart_monitor) s.and(this);
    }

    /**
     * This method indicates if a state is active.
     */
    public boolean active() {
        return _active;
    }

    /**
     * Internal use only; This method computes the "brother" state, if any, of a
     * state. If the type of the state is <CODE>AbstractStatechart</CODE>, the
     * returned value cannot be <CODE>null</CODE>.
     *
     * @return The "brother" state or <CODE>null</CODE> if this state is an
     * instance of <CODE>AbstractStatechart_monitor</CODE> (<I>i.e.</I>, an
     * instance of a state machine).
     */
    protected AbstractStatechart brother() {
        if (_node == null) {
            return null;
        }
        return this == _node._left ? _node._right : _node._left;
    }

    /**
     * Internal use only; This method computes the closest superstate of two
     * states. The common superstate may be the state machine itself
     * (<I>i.e.</I>, an instance of {@link AbstractStatechart_monitor}).
     */
    protected AbstractStatechart commonSuperWith(AbstractStatechart s) {
        if (s == null) {
            return null;
        }
        if (s == this) {
            return _node;
        }
        Vector supers = new Vector();
        for (AbstractStatechart i = this; i != null; i = i._node) {
            supers.addElement(i);
        }
        AbstractStatechart commonSuperState = s;
        while (commonSuperState != null && !supers.contains(commonSuperState)) {
            commonSuperState = commonSuperState._node;
        }
        return commonSuperState;
    }

    /**
     * This method indicates whether the argument state is a direct or indirect
     * superstate of <CODE>this</CODE>.
     */
    public boolean hasForSuperState(AbstractStatechart s) {
        if (s == null) {
            return false;
        }
        if (s == _node) {
            return true;
        }
        return _node != null ? _node.hasForSuperState(s) : false;
    }

    /**
     * This method indicates if two states are orthogonal.
     */
    public boolean isAndWith(AbstractStatechart s) {
        if (s == null || s == this || s.hasForSuperState(this) || this.hasForSuperState(s)) {
            return false;
        }
        AbstractStatechart commonSuperState = this.commonSuperWith(s);
        if (commonSuperState == null) {
            return false;
        }
        return (commonSuperState._xor == false);
    }

    /**
     * This method indicates if two states are exclusive.
     */
    public boolean isXorWith(AbstractStatechart s) {
        if (s == null || s == this || s.hasForSuperState(this) || this.hasForSuperState(s)) {
            return false;
        }
        AbstractStatechart commonSuperState = this.commonSuperWith(s);
        if (commonSuperState == null) {
            return false;
        }
        return (commonSuperState._xor == true);
    }

    /**
     * This method amounts to calling
     * <CODE> allowedEvent(event,object,action,null);</CODE>.
     */
    public void allowedEvent(String event, Object object, String action) {
        allowedEvent(event, object, action, null);
    }

    /**
     * This method amounts to calling
     * <CODE>allowedEvent(event,object,action,args,AbstractStatechart.No_reentrance);</CODE>.
     */
    public void allowedEvent(String event, Object object, String action, Object[] args) {
        allowedEvent(event, object, action, args, AbstractStatechart.No_reentrance);
    }

    /**
     * This method amounts to calling
     * <CODE>allowedEvent(event,null,"true",object,action,args,reentrance_mode);</CODE>.
     */
    public void allowedEvent(String event, Object object, String action, Object[] args, byte reentrance_mode) {
        allowedEvent(event, null, "true", object, action, args, reentrance_mode);
    }

    /**
     * This method amounts to calling
     * <CODE>allowedEvent(event,guard_object,guard_action,null,object,action,null,AbstractStatechart.No_reentrance);</CODE>.
     */
    public void allowedEvent(String event, Object guard_object, String guard_action, Object object, String action) {
        allowedEvent(event, guard_object, guard_action, null, object, action, null, AbstractStatechart.No_reentrance);
    }

    /**
     * This method amounts to calling
     * <CODE>allowedEvent(event,guard_object,guard_action,null,object,action,null,reentrance_mode);</CODE>.
     */
    public void allowedEvent(String event, Object guard_object, String guard_action, Object object, String action, byte reentrance_mode) {
        allowedEvent(event, guard_object, guard_action, null, object, action, null, reentrance_mode);
    }

    /**
     * This method amounts to calling
     * <CODE>allowedEvent(event,guard_object,guard_action,null,object,action,args,AbstractStatechart.No_reentrance);</CODE>.
     */
    public void allowedEvent(String event, Object guard_object, String guard_action, Object object, String action, Object[] args) {
        allowedEvent(event, guard_object, guard_action, null, object, action, args, AbstractStatechart.No_reentrance);
    }

    /**
     * This method amounts to calling
     * <CODE>allowedEvent(event,guard_object,guard_action,null,object,action,args,reentrance_mode);</CODE>.
     */
    public void allowedEvent(String event, Object guard_object, String guard_action, Object object, String action, Object[] args, byte reentrance_mode) {
        allowedEvent(event, guard_object, guard_action, null, object, action, args, reentrance_mode);
    }

    /**
     * This method amounts to calling
     * <CODE>allowedEvent(event,guard_object,guard_action,guard_args,object,action,null);</CODE>.
     */
    public void allowedEvent(String event, Object guard_object, String guard_action, Object[] guard_args, Object object, String action) {
        allowedEvent(event, guard_object, guard_action, guard_args, object, action, null);
    }

    /**
     * This method amounts to calling
     * <CODE>allowedEvent(event,guard_object,guard_action,guard_args,object,action,null,reentrance_mode);</CODE>.
     */
    public void allowedEvent(String event, Object guard_object, String guard_action, Object[] guard_args, Object object, String action, byte reentrance_mode) {
        allowedEvent(event, guard_object, guard_action, guard_args, object, action, null, reentrance_mode);
    }

    /**
     * This method amounts to calling
     * <CODE>allowedEvent(event,guard_object,guard_action,guard_args,object,action,args,AbstractStatechart.No_reentrance);</CODE>.
     */
    public void allowedEvent(String event, Object guard_object, String guard_action, Object[] guard_args, Object object, String action, Object[] args) {
        allowedEvent(event, guard_object, guard_action, guard_args, object, action, args, AbstractStatechart.No_reentrance);
    }

    /**
     * This method registers the allowed events for a given state.
     */
    public void allowedEvent(String event, Object guard_object, String guard_action, Object[] guard_args, Object object, String action, Object[] args, byte reentrance_mode) {
        if (event == null) {
            return;
        }
        if (event.equals(Pseudo_event)) {
            return;
        }
        AbstractGuard guard = guard(guard_object, guard_action, guard_args);
        Hashtable guards = (Hashtable) _allowed_events.get(event);
        Vector actions;
        if (guards == null) {
            guards = new Hashtable();
            actions = new Vector();
            guards.put(guard, actions);
            _allowed_events.put(event, guards);
        } else {
            actions = (Vector) guards.remove(guard);
            if (actions == null) {
                actions = new Vector();
            }
            guards.put(guard, actions);
        }
        if (object != null && action != null) {
            AbstractAction new_action = action(object, action, args, reentrance_mode);
            /**
             * Removed from ver. 1.3
             */
//            Boolean value = (Boolean) _runtime_events.get(event);
//            if (value != null) {
//                if (value == Boolean.TRUE) {
//                    actions.removeAllElements();
//                    _runtime_events.put(event, Boolean.FALSE);
//                }
//            }
            /**
             * End of removed from ver. 1.3
             */
            // The following statements rely on a specific implementation of the 'public boolean equals(Object action)' method in the 'AbstractAction' class:
            int index = actions.indexOf(new_action); // Search of first index is safe because duplicates have not been allowed
            if (index != -1) {
                actions.removeElementAt(index); // Arguments' update, e.g., 'search' event in 'Stack' component
                actions.add(index, new_action);
            } else {
                actions.addElement(new_action);
            }
        }
    }

    /**
     * This method executes the actions of allowed events, which are eligible
     * during a run-to-completion cycle; For internal use only.
     */
    protected void allowedEvent(String event, Hashtable execution, Vector eliminated_transitions, StringBuffer verbose) {
        if (_left != null) {
            _left.allowedEvent(event, execution, eliminated_transitions, verbose);
        }
        if (_right != null) {
            _right.allowedEvent(event, execution, eliminated_transitions, verbose);
        }
        Hashtable guards = (Hashtable) _allowed_events.get(event);
        if (guards != null) {
            /**
             * Removed from ver. 1.3
             */
//            Boolean value = (Boolean) _runtime_events.get(event);
//            if (value == null || (value != null && value == Boolean.FALSE)) {
//                _runtime_events.put(event, Boolean.TRUE);
//            }
            /**
             * End od removed from ver. 1.3
             */
            if (_active) {
                for (Enumeration elements = guards.keys(); elements.hasMoreElements();) {
                    AbstractGuard guard = (AbstractGuard) elements.nextElement();
                    try {
                        if (guard.execute()) {
                            boolean execute = true;
                            for (Enumeration eligible_transitions = execution.keys(); eligible_transitions.hasMoreElements();) {
                                Transition transition = (Transition) eligible_transitions.nextElement();
                                AbstractStatechart from = transition._from;
                                AbstractStatechart to = transition._to;
                                if (this == from) {
                                    verbose.append("\t\t{WARNING}overridden by allowed event: " + from._name + " -> " + to._name + ", guard: [true]\n");
                                    if (!eliminated_transitions.contains(transition)) {
                                        eliminated_transitions.addElement(transition);
                                    }
                                }
                                if (from.hasForSuperState(this)) {
                                    execute = false;
                                }
                            }
                            if (execute) {
                                Vector executed_actions = (Vector) guards.get(guard);
                                for (int i = 0; i < executed_actions.size(); i++) {
                                    AbstractAction executed_action = (AbstractAction) executed_actions.elementAt(i);
                                    try {
                                        executed_action.execute();
                                    } catch (Statechart_exception se) {
                                    } finally {
                                        verbose.append(Allowed_event_display_message + event + "/" + executed_action.verbose() + "\n");
                                        /**
                                         * Lea
                                         */
                                        if(PauWareManager.isTraceable())
                                        	PauWareManager.trace.onAllowedEvent(this, executed_action);
                                    }
                                }
                            }
                        }
                    } catch (Statechart_exception se) {
                        verbose.append("\t{WARNING}canceled (allowed event): " + _name + ", guard: [" + guard.verbose() + "]\n");
                    }
                }
            }
        }
    }

    /**
     * This method returns the <I>allowed events/</I> actions of the state.
     *
     * @return The <I>allow:</I> actions (if any) in a printable form
     */
    public String get_allowed_events_label() {
        if (_allowed_events.isEmpty()) {
            return null;
        }
        String allowed_events_label = "";
        java.util.Enumeration allowed_events = _allowed_events.keys();
        while (allowed_events.hasMoreElements()) {
            String event = allowed_events.nextElement().toString();
            java.util.Enumeration guarded_actions = ((java.util.Hashtable) _allowed_events.get(event)).keys();
            while (guarded_actions.hasMoreElements()) {
                AbstractGuard guard = (AbstractGuard) guarded_actions.nextElement();
                java.util.Vector actions = (java.util.Vector) ((java.util.Hashtable) _allowed_events.get(event)).get(guard);
                for (int i = 0; i < actions.size(); i++) {
                    AbstractAction action = (AbstractAction) actions.elementAt(i);
                    allowed_events_label += Allow_header_text + event + guard.to_UML() + '/' + action.to_UML() + '\n';
                }
            }
        }
        return allowed_events_label;
    }
    /**
     * This variable represents the fact that a state is or not a history state.
     *
     * @see AbstractStatechart#shallow_history()
     * @see AbstractStatechart#deep_history()
     */
    private boolean _is_history = false;
    /**
     * This variable records the history of a state by pointing to its last (if
     * any) visited substate. Possible values are between <CODE>_left</CODE> and
     * <CODE>_right</CODE>.
     *
     * @see AbstractStatechart#deep_history()
     * @see AbstractStatechart#shallow_history()
     */
    private AbstractStatechart _history = null;

    /**
     * This method amounts to configuring a state as a (shallow) history state:
     * <I>H</I> notation. History setup is only possible after full state
     * machine structuring.
     */
    public void shallow_history() {
        if (!_is_history) {
            _is_history = true;
        }
    }

    /**
     * This method amounts to configuring a state as a (deep) history state:
     * <I>H*</I> notation. History setup is only possible after full state
     * machine structuring.
     */
    public void deep_history() {
        shallow_history();
        if (_left != null) {
            _left.deep_history();
        }
        if (_right != null) {
            _right.deep_history();
        }
    }

    /**
     * This method tests if a state is a (shallow) history state.
     */
    public boolean is_shallow_history() {
        if (_is_history == false || leaf() || is_deep_history()) {
            return false;
        }
        return check_shallow_history(null);
    }

    /**
     * This method checks if a state is a (shallow) history state.
     */
    private boolean check_shallow_history(AbstractStatechart s) {
        return _node == null ? true : _node._is_history == false;
    }

    /**
     * This method tests if a state is a (deep) history state.
     */
    public boolean is_deep_history() {
        if (_is_history == false || leaf()) {
            return false;
        }
        return check_deep_history(null);
    }

    /**
     * This (internal) method checks if a state is a (deep) history state.
     */
    private boolean check_deep_history(AbstractStatechart s) {
        if (leaf()) {
            return true;
        }
        boolean any_super_is_a_history_state = false;
        for (AbstractStatechart node = _node; node != null && node != s; node = node._node) {
            any_super_is_a_history_state |= node._is_history;
        }
        if (any_super_is_a_history_state == true) {
            return false;
        }
        return _left.check_deep_history(this) && _right.check_deep_history(this);
    }

    /**
     * This method amounts to calling
     * <CODE>doActivity(object,activity,null);</CODE>.
     */
    public AbstractStatechart doActivity(Object object, String activity) {
        return doActivity(object, activity, null);
    }

    /**
     * This method associates an activity <I>a</I> with a state according to the
     * following UML formalism: <I>do/ a</I>. Only one activity may be setup for
     * the same state. Normally, activities are not intended to communicate with
     * other software components. It is thus preferable to encapsulate local
     * data transformations in activities. Such transformations are not supposed
     * to require any control, synchronization, etc.
     *
     * @return The state itself
     */
    public AbstractStatechart doActivity(Object object, String activity, Object[] args) {
        _do = activity(object, activity, args);
        return this;
    }

    /**
     * This method returns the <I>do/ </I>activity of the state.
     *
     * @return The <I>do/ </I> activity (if any) in a printable form.
     */
    public String get_do_label() {
        // 'SendSignalAction_symbol' is removed as first character for 'do' activities:
        return _do == null ? null : Do_header_text + _do.to_UML().substring(1);
    }

    /**
     * This method amounts to calling
     * <CODE>set_entryAction(object,action,null);</CODE>.
     */
    public AbstractStatechart set_entryAction(Object object, String action) {
        return set_entryAction(object, action, null);
    }

    /**
     * This method amounts to calling
     * <CODE>set_entryAction(object,action,args,AbstractStatechart.No_reentrance);</CODE>.
     */
    public AbstractStatechart set_entryAction(Object object, String action, Object[] args) {
        return set_entryAction(object, action, args, AbstractStatechart.No_reentrance);
    }

    /**
     * This method associates an action <I>a</I> with a state according to the
     * following UML formalism: <I>entry/ a</I>. Multiple actions for the same
     * state require multiple uses of this method.
     *
     * @param reentrance_mode The {@link AbstractStatechart#Reentrance} class
     * variable may be used as a value for this parameter; This value prevents
     * unanticipated reentrance
     * @return The state itself
     */
    public AbstractStatechart set_entryAction(Object object, String action, Object[] args, byte reentrance_mode) {
        AbstractAction abstract_action = action(object, action, args, reentrance_mode);
        _entry.addElement(abstract_action);
        return this;
    }

    /**
     * This method amounts to calling
     * <CODE>reset_entryAction(object,action,null);</CODE>.
     */
    public AbstractStatechart reset_entryAction(Object object, String action) {
        return reset_entryAction(object, action, null);
    }

    /**
     * This method amounts to calling
     * <CODE>reset_entryAction(object,action,args,AbstractStatechart.No_reentrance);</CODE>.
     */
    public AbstractStatechart reset_entryAction(Object object, String action, Object[] args) {
        return reset_entryAction(object, action, args, AbstractStatechart.No_reentrance);
    }

    /**
     * This method associates an action <I>a</I> with a state according to the
     * following UML formalism: <I>entry/ a</I>. This method aims at erasing an
     * already recorded entry action due to the change of the
     * <CODE>object</CODE> and/or <CODE>args</CODE> parameters.
     *
     * @param reentrance_mode The {@link AbstractStatechart#Reentrance} class
     * variable may be used as a value for this parameter; This value prevents
     * unanticipated reentrance
     * @return The state itself
     */
    public AbstractStatechart reset_entryAction(Object object, String action, Object[] args, byte reentrance_mode) {
        int i = 0;
        while (i < _entry.size()) {
            AbstractAction a = (AbstractAction) _entry.elementAt(i);
            if (a._action.equals(action) && a.compare_args(args)) {
                break;
            }
            i++;
        }
        if (i == _entry.size()) {
            set_entryAction(object, action, args, reentrance_mode);
        } else {
            _entry.setElementAt(action(object, action, args, reentrance_mode), i);
        }
        return this;
    }

    /**
     * This method returns the <I>entry/ </I>actions of the state.
     *
     * @return The <I>entry/ </I>actions (if any) in a printable form.
     */
    private String get_entries_label() {
        if (_entry.isEmpty()) {
            return null;
        }
        String entries = "";
        for (int i = 0; i < _entry.size(); i++) {
            entries += Entry_header_text + ((AbstractAction) _entry.elementAt(i)).to_UML() + '\n';
        }
        return entries;
    }

    /**
     * This method amounts to calling
     * <CODE>set_exitAction(object,action,null);</CODE>.
     */
    public AbstractStatechart set_exitAction(Object object, String action) {
        return set_exitAction(object, action, null);
    }

    /**
     * This method amounts to calling
     * <CODE>set_exitAction(object,action,args,AbstractStatechart.No_reentrance);</CODE>.
     */
    public AbstractStatechart set_exitAction(Object object, String action, Object[] args) {
        return set_exitAction(object, action, args, AbstractStatechart.No_reentrance);
    }

    /**
     * This method associates an action <I>a</I> with a state according to the
     * following UML formalism: <I>exit/ a</I>. Multiple actions for the same
     * state require multiple uses of this method.
     *
     * @param reentrance_mode The {@link AbstractStatechart#Reentrance} class
     * variable may be used as a value for this parameter; This value prevents
     * unanticipated reentrance
     * @return The state itself
     */
    public AbstractStatechart set_exitAction(Object object, String action, Object[] args, byte reentrance_mode) {
        AbstractAction abstract_action = action(object, action, args, reentrance_mode);
        _exit.addElement(abstract_action);
        return this;
    }

    /**
     * This method amounts to calling
     * <CODE>reset_exitAction(object,action,null);</CODE>.
     */
    public AbstractStatechart reset_exitAction(Object object, String action) {
        return reset_exitAction(object, action, null);
    }

    /**
     * This method amounts to calling
     * <CODE>reset_exitAction(object,action,args,AbstractStatechart.No_reentrance);</CODE>.
     */
    public AbstractStatechart reset_exitAction(Object object, String action, Object[] args) {
        return reset_exitAction(object, action, args, AbstractStatechart.No_reentrance);
    }

    /**
     * This method associates an action <I>a</I> with a state according to the
     * following UML formalism: <I>exit/ a</I>. This method aims at erasing an
     * already recorded exit action due to the change of the <CODE>object</CODE>
     * and/or <CODE>args</CODE> parameters.
     *
     * @param reentrance_mode The {@link AbstractStatechart#Reentrance} class
     * variable may be used as a value for this parameter; This value prevents
     * unanticipated reentrance
     * @return The state itself
     */
    public AbstractStatechart reset_exitAction(Object object, String action, Object[] args, byte reentrance_mode) {
        int i = 0;
        while (i < _exit.size()) {
            AbstractAction a = (AbstractAction) _exit.elementAt(i);
            if (a._action.equals(action) && a.compare_args(args)) {
                break;
            }
            i++;
        }
        if (i == _exit.size()) {
            set_exitAction(object, action, args, reentrance_mode);
        } else {
            _exit.setElementAt(action(object, action, args, reentrance_mode), i);
        }
        return this;
    }

    /**
     * This method returns the <I>exit/ </I>actions of the state.
     *
     * @return The <I>exit/ </I>actions (if any) in a printable form.
     */
    private String get_exits_label() {
        if (_exit.isEmpty()) {
            return null;
        }
        String exits = "";
        for (int i = 0; i < _exit.size(); i++) {
            exits += Exit_header_text + ((AbstractAction) _exit.elementAt(i)).to_UML() + '\n';
        }
        return exits;
    }

    /**
     * This method amounts to calling
     * <CODE>stateInvariant(object,invariant,null);</CODE>.
     */
    public AbstractStatechart stateInvariant(Object object, String invariant) {
        return stateInvariant(object, invariant, null);
    }

    /**
     * This method associates an invariant with a state as proposed by the UML
     * formalism. Only one invariant may be setup for the same state. An
     * invariant is evaluated during a run-to-completion cycle by means of a
     * (dedicated) executed action.
     *
     * @param object The object in charge of executing the action, corresponding
     * to the invariant to be evaluated
     * @param invariant The action to be executed, corresponding to the
     * invariant to be evaluated
     * @param args The action's arguments, if any
     * @return The state itself
     * @see
     * com.pauware.pauware_engine._Core.AbstractStatechart_monitor#run_to_completion(String,boolean)
     */
    public AbstractStatechart stateInvariant(Object object, String invariant, Object[] args) {
        _invariant = action(object, invariant, args, No_reentrance);
        return this;
    }

    /**
     * Internal use only; This method computes the invariant of a state, this
     * state being active or not at call time.
     *
     * @return The result of evaluating the state's invariant by calling the
     * Java method, if any, attached to this state as invariant evaluator This
     * methods returns <CODE>true</CODE> if there is no attached evaluation
     * method or the latter does not return a Boolean type and no exception
     * occurs when calling it In contrast, this methods raises an exception if
     * the state still lasts (its attached activity is not terminated) or an
     * exception occurs when evaluating the invariant
     * @see AbstractStatechart#stateInvariant(Object,String,Object[])
     * @see AbstractStatechart#deepStateInvariant()
     */
    protected String shallowStateInvariant() throws Statechart_exception {
        if (_do != null) {
            _do.wait_for_completion();
        }
        if (Pseudo_state.equals(_name) || _invariant == null) {
            return "";
        }
        try {
            _invariant.execute();
            
            /**
             * Lea
             */
            if(PauWareManager.isTraceable()) {
                if(_invariant.printable_result().equals("true")) {
                    PauWareManager.trace.onVerifiedInvariant(this, _invariant);
                } else {
                    PauWareManager.trace.onViolatedInvariant(this, _invariant);
                }
            }
            
            return get_invariant_label() + Textual_view_subject_separator + _invariant.printable_result();
        } catch (Statechart_exception se) {
                    
            throw new Statechart_exception("Invariant cannot be evaluated since invariant evaluator failed: " + _invariant.verbose());
        }
    }

    /**
     * Internal use only; This method computes the global invariant of a state
     * machine. This invariant is equal to a logical <I>and</I> for all of the
     * invariants of its <b>active</b> states.
     *
     * @see AbstractStatechart#stateInvariant(Object,String,Object[])
     * @see AbstractStatechart#shallowStateInvariant()
     * @see
     * com.pauware.pauware_engine._Core.AbstractStatechart_monitor#run_to_completion(String,boolean)
     */
    protected String deepStateInvariant() throws Statechart_exception {
        if (_active) {
            String result = shallowStateInvariant();
            result += result.equals("") ? "" : "\n\t\t" + result;
            if (_left != null && _right != null) {
                result += _left.deepStateInvariant() + _right.deepStateInvariant();;
            }
            return result;
        }
        return "";
    }

    /**
     * This method returns the <I>invariant: </I>action of the state.
     *
     * @return The <I>invariant: </I>action (if any) in a printable form.
     */
    public String get_invariant_label() {
        return _invariant == null ? null : Invariant_header_text + '[' + _invariant.to_UML() + ']';
    }

    /**
     * Internal use only; This method is called at state machine starting time.
     * It is also called within any run-to-completion cycle. In this latter
     * case, this method is called by
     * {@link AbstractStatechart_monitor#activate(AbstractStatechart,AbstractStatechart,StringBuffer)}.
     */
    protected void entry(Stack state_execution_flow, StringBuffer verbose) throws Statechart_exception {
        // Recursive method, first call: 'state_execution_flow' is a 'Stack' object such that its bottom is equal to '_to', the state to be reached in a transition from '_from' to '_to'
        if ((_left == null && _right != null) || (_left != null && _right == null)) {
            throw new Statechart_state_based_exception("Incorrect state entry configuration (unexpected 'null')", _left, _right);
        }
        if (_active == false) { // When already active, this stops recursion
            
            /**
             * Lea
             */
            if(PauWareManager.isTraceable())
                PauWareManager.trace.onStateEntry(this);
            
            try {
                for (int i = 0; i < _entry.size(); i++) {
                    AbstractAction abstractAction = (AbstractAction) _entry.elementAt(i);
                    abstractAction.execute();
                    verbose.append(Entry_action_display_message + abstractAction.verbose() + "\n");
                
                    /**
                     * Lea
                     */
                    if(PauWareManager.isTraceable())
                    	PauWareManager.trace.onEntryAction(this, abstractAction);
                }
            } finally {
                if (_left != null && _right != null) { // Leaf state, stop recursion
                    if (_left.isAndWith(_right)) {
                        AbstractStatechart s = null;
                        if (state_execution_flow != null) {
                            if (state_execution_flow.empty()) {
                                _left.entry(null, verbose);
                                _right.entry(null, verbose);
                            } else {
                                s = (AbstractStatechart) state_execution_flow.pop();
                                if (_left == s) {
                                    _left.entry(state_execution_flow, verbose);
                                    _right.entry(null, verbose);
                                } else {
                                    if (_right == s) {
                                        _left.entry(null, verbose);
                                        _right.entry(state_execution_flow, verbose);
                                    } else {
                                        throw new Statechart_state_based_exception("Incorrect state execution flow (neither '_left' nor '_right' is on the flow)", _left, _right);
                                    }
                                }
                            }
                        } else {
                            _left.entry(null, verbose);
                            _right.entry(null, verbose);
                        }
                    } else {
                        if (_left.isXorWith(_right)) {
                            if (state_execution_flow != null) { // 'entry(state_execution_flow)' has been called within the context of a state execution flow, input and history states are bypassed
                                AbstractStatechart s = null;
                                if (state_execution_flow.empty()) {
                                    if (_left.isInputState()) {
                                        if (_right.isInputState()) {
                                            throw new Statechart_state_based_exception("Incorrect input state configuration (two contradicting default input states)", _left, _right);
                                        } else {
                                            // '_left' is input state while '_right' is not
                                            /**
                                             * History issues (Dec. 2012)
                                             */
                                            if (!_is_history) {
                                                _left.entry(null, verbose);
                                            } else {
                                                // 'this' is a history state:
                                                if (_history == null) {
                                                    // first time, there is no recorded history
                                                    _left.entry(null, verbose);
                                                } else {
                                                    _history.entry(null, verbose);
                                                }
                                            }
                                            /**
                                             * End of history issues (Dec. 2012)
                                             */
                                        }
                                    }
                                    if (_right.isInputState()) {
                                        if (_left.isInputState()) {
                                            throw new Statechart_state_based_exception("Incorrect input state configuration (two contradicting default input states)", _left, _right);
                                        } else {
                                            // '_right' is input state while '_left' is not
                                            /**
                                             * History issues (Dec. 2012)
                                             */
                                            if (!_is_history) {
                                                _right.entry(null, verbose);
                                            } else {
                                                // 'this' is a history state:
                                                if (_history == null) {
                                                    // first time, there is no recorded history
                                                    _right.entry(null, verbose);
                                                } else {
                                                    _history.entry(null, verbose);
                                                }
                                            }
                                            /**
                                             * End of history issues (Dec. 2012)
                                             */
                                        }
                                    }
                                } else {
                                    s = (AbstractStatechart) state_execution_flow.pop();
                                    if (_left == s) {
                                        _left.entry(state_execution_flow, verbose);
                                    } else {
                                        if (_right == s) {
                                            _right.entry(state_execution_flow, verbose);
                                        } else {
                                            throw new Statechart_state_based_exception("Incorrect state execution flow (neither '_left' nor '_right' is on the flow)", _left, _right);
                                        }
                                    }
                                }
                            } else { // 'entry(state_execution_flow)' has NOT been called within the context of a state execution flow, input and history states are handled
                                if (_left.isInputState()) {
                                    if (_right.isInputState()) {
                                        throw new Statechart_state_based_exception("Incorrect input state configuration (two contradicting default input states)", _left, _right);
                                    } else {
                                        // '_left' is input state while '_right' is not
                                        /**
                                         * History issues (Dec. 2012)
                                         */
                                        if (!_is_history) {
                                            _left.entry(null, verbose);
                                        } else {
                                            // 'this' is a history state:
                                            if (_history == null) {
                                                // first time, there is no recorded history
                                                _left.entry(null, verbose);
                                            } else {
                                                _history.entry(null, verbose);
                                            }
                                        }
                                        /**
                                         * End of history issues (Dec. 2012)
                                         */
                                    }
                                }
                                if (_right.isInputState()) {
                                    if (_left.isInputState()) {
                                        throw new Statechart_state_based_exception("Incorrect input state configuration (two contradicting default input states)", _left, _right);
                                    } else {
                                        // '_right' is input state while '_left' is not
                                        /**
                                         * History issues (Dec. 2012)
                                         */
                                        if (!_is_history) {
                                            _right.entry(null, verbose);
                                        } else {
                                            // 'this' is a history state:
                                            if (_history == null) {
                                                // first time, there is no recorded history
                                                _right.entry(null, verbose);
                                            } else {
                                                _history.entry(null, verbose);
                                            }
                                        }
                                        /**
                                         * End of history issues (Dec. 2012)
                                         */
                                    }
                                }
                            }

                        } else {
                            throw new Statechart_state_based_exception("Incorrect state entry configuration (neither 'and' nor 'xor'", _left, _right);
                        }
                    }
                }
                try {
                    if (_do != null) {
                        _do.execute();
                        verbose.append(Do_activity_display_message + _do.verbose().substring(1) + "\n");
                    
                        /**
                         * Lea
                         */
                        if(PauWareManager.isTraceable())
                        	PauWareManager.trace.onDoActivity(this, _do);
                    }
                } finally {
                    _active = true;
                    if (!_name.equals(Pseudo_state)) {
                        verbose.append(Activated_state_display_message + _name + "\n");
                    }
                }
            }
        }
    }

    /**
     * Internal use only; This method is called at state machine stopping time
     * for any run-to-completion cycle. This method is called by
     * {@link AbstractStatechart_monitor#disactivate(AbstractStatechart,AbstractStatechart,StringBuffer)}.
     */
    protected boolean exit(StringBuffer verbose) throws Statechart_exception {
        if ((_left == null && _right != null) || (_left != null && _right == null)) {
            throw new Statechart_state_based_exception("Incorrect state exit configuration (unexpected 'null')", _left, _right);
        }
        if (_active == true) {
            /**
             * The activity, if any, attached to the state must be fully
             * completed
             */
            if (_do != null) {
                _do.wait_for_completion();
            }
            if (_left != null && _right != null) {
                /**
                 * History issues (Dec. 2012)
                 */
                boolean left_exit = _left.exit(verbose);
                boolean right_exit = _right.exit(verbose);
                if (left_exit && !right_exit) {
                    _history = _left;
                }
                if (right_exit && !left_exit) {
                    _history = _right;
                }
                /**
                 * End of history issues (Dec. 2012)
                 */
            }
            try {
                // while (isLasting()) {/* semantic variation point: this may lead to a;d;((e;f)||b);c instead of a;d;(b||e);f;c */}
                for (int i = 0; i < _exit.size(); i++) {
                    AbstractAction abstractAction = (AbstractAction) _exit.elementAt(i);
                    abstractAction.execute();
                    verbose.append(Exit_action_display_message + abstractAction.verbose() + "\n");
                    /**
                     * Lea
                     */
                    if(PauWareManager.isTraceable())
                    	PauWareManager.trace.onExitAction(this, abstractAction);
                }
            } finally {
                _active = false;
                if (!_name.equals(Pseudo_state)) {
                    verbose.append(Disactivated_state_display_message + _name + "\n");
                
                    /**
                     * Lea
                     */
                    if(PauWareManager.isTraceable())
                    	PauWareManager.trace.onStateExit(this);
                }
            }
            // Dec. 2012. One has really exited from 'this':
            return true;
        } else {
            // Dec. 2012. One has not exited from 'this' because 'this' was inactive when calling 'exit':
            return false;
        }
    }

    /**
     * This method forces a state to be an input state. The UML notation is a
     * black point with an arrow pointing to the state. At state machine
     * creation time, some states of a state machine must be declared as input
     * states in order to guarantee the state machine's determinism.
     *
     * @see
     * AbstractStatechart_monitor#AbstractStatechart_monitor(AbstractStatechart,String,boolean)
     */
    public void inputState() {
        if (isOutputState()) {
            return;
        }
        _inputState = true;
    }

    /**
     * This method tests if a state is an input state.
     */
    public boolean isInputState() {
        return _inputState;
    }

    /**
     * This method enables the naming of a state. <b>Caution</b>: there is
     * <b>no</b> naming checking in <I>PauWare</I>. For instance, naming two
     * states with the same name would probably lead to unpredictable behaviors,
     * even defects.
     */
    public AbstractStatechart name(String name) {
        if (name != null) {
            _name = name;
        }
        return this;
    }

    /**
     * This method forces a state to be an output state. The UML notation is a
     * black point surrounded by a circle which is pointed at by an arrow coming
     * out of another state. This method has no effect for the moment; It will
     * be used in a forthcoming API.
     */
    public void outputState() {
        if (isInputState()) {
            return;
        }
        _outputState = true;
    }

    /**
     * Completion transitions (i.e., those without any event label) are
     * automatically triggered when reaching output states. Since ver. 1.3.
     */
    protected void set_completion(AbstractStatechart s) {
        if (_outputState) {
            set_entryAction(s, Completion, null, Reentrance);
        }
        if (_left != null) {
            _left.set_completion(s);
        }
        if (_right != null) {
            _right.set_completion(s);
        }
    }

    /**
     * This method tests if a state is an output state.
     */
    public boolean isOutputState() {
        return _outputState;
    }

    /**
     * This method answers the question: is this state a leaf state
     * (<I>i.e.</I>, without direct or indirect substates) of the state machine?
     */
    public boolean leaf() {
        return _left == null && _right == null;
    }

    /**
     * This method answers the question: is this state the most outer state of
     * the state machine? The answer must always be <CODE>false</CODE> for an
     * instance of the <CODE>AbstractStatechart</CODE> type.
     */
    public boolean root() throws Statechart_state_based_exception {
        if (_node == this) {
            throw new Statechart_state_based_exception("Root error:", _node, this);
        }
        return false;
    }

    /**
     * Management facility: this method is a utility for runtime checking. It
     * verifies if an active state, which has its "brother" state <b>active</b>,
     * shares a orthogonality relationship with it. It also verifies if an
     * active state, which has its "brother" state <b>inactive</b>, shares an
     * exclusion relationship with it.
     */
    public void verify() throws Statechart_state_based_exception {
        if (isXorWith(brother()) && _active && brother()._active) {
            throw new Statechart_state_based_exception("Xor error: ", this, brother());
        }
        if (isAndWith(brother()) && _active && !brother()._active) {
            throw new Statechart_state_based_exception("And error: ", this, brother());
        }
    }

    /**
     * Management facility: this method is offered by a software component,
     * which implements the <CODE>Manageable_base</CODE> interface.
     *
     * @see com.pauware.pauware_engine._Core.Manageable_base
     */
    public String current_state() {
        String name = "";
        if (_active) {
            if (leaf()) {
                name = _name;
            } else {
                String left_name = _left.current_state();
                String right_name = _right.current_state();
                if (_left.isAndWith(_right)) {
                    if (!left_name.equals(Pseudo_state)) {
                        name = "(" + left_name;
                        if (!right_name.equals(Pseudo_state)) {
                            name += ".AND." + right_name + ")";
                        } else {
                            name += ")";
                        }
                    } else {
                        if (!right_name.equals(Pseudo_state)) {
                            name += "(" + right_name + ")";
                        }
                    }
                } else {
                    if (_left._active) {
                        name = "(" + left_name + ")";
                    } else {
                        name = "(" + right_name + ")";
                    }
                }
                if (_name != Pseudo_state) {
                    name = _name + name;
                }
            }
        }
        return name;
    }

    /**
     * Management facility: this method is offered by a software component,
     * which implements the <CODE>Manageable_base</CODE> interface.
     *
     * @see com.pauware.pauware_engine._Core.Manageable_base
     */
    public boolean in_state(String name) {
        return current_state().indexOf("(" + name) != -1;
    }

    /**
     * This method returns the name of a state.
     *
     * @return The returned value can be the value of the
     * {@link AbstractStatechart#Pseudo_state} class variable; One has however
     * to notice that pseudo-states do not require direct manipulation
     */
    public String name() {
        // The '_name' field is by construction never equal to 'null'
        return _name;
    }
    
   /**
     * This method returns the name of a state.
     *
     * @return The returned value can be the value of the
     * {@link AbstractStatechart#Pseudo_state} class variable; One has however
     * to notice that pseudo-states do not require direct manipulation
     */
    public String getName() {
        return this.name();
    }

    /**
     * Internal use only; This method is called by
     * {@link AbstractStatechart_monitor#to_state(String)}.
     *
     * @return The state to be reached, if any
     */
    protected AbstractStatechart lookup(String name) throws Statechart_exception {
        // The '_name' field is by construction never equal to 'null'
        if (_name.equals(name)) {
            return this;
        } else {
            if (leaf()) {
                return null;
            } else {
                AbstractStatechart to = _left.lookup(name);
                if (to == null) {
                    return _right.lookup(name);
                } else {
                    return to;
                }
            }
        }
    }
    /**
     * For trace and visualization purposes: this class variable is used by the
     * verbose mode. It is also used by
     * <I>PauWare view</I> tool to control what is displayed on the HTML page.
     * Since ver. 1.3.
     */
    public static final String Allow_header_text = "allow: ";
    /**
     * For trace and visualization purposes: this class variable is used by the
     * verbose mode. It is also used by
     * <I>PauWare view</I> tool to control what is displayed on the HTML page.
     * Since ver. 1.3.
     */
    public static final String Exit_header_text = "exit/ ";
    /**
     * For trace and visualization purposes: this class variable is used by the
     * verbose mode. It is also used by
     * <I>PauWare view</I> tool to control what is displayed on the HTML page.
     * Since ver. 1.3.
     */
    public static final String Entry_header_text = "entry/ ";
    /**
     * For trace and visualization purposes: this class variable is used by the
     * verbose mode. It is also used by
     * <I>PauWare view</I> tool to control what is displayed on the HTML page.
     * Since ver. 1.3.
     */
    public static final String Do_header_text = "do/ ";
    /**
     * For trace and visualization purposes: this class variable is used by the
     * verbose mode. It is also used by
     * <I>PauWare view</I> tool to control what is displayed on the HTML page.
     * Since ver. 1.3.
     */
    public static final String Invariant_header_text = "invariant: ";

    /**
     * For trace and visualization purposes: this class variable is used by the
     * verbose mode. It is also used by
     * <I>PauWare view</I> tool to control what is displayed on the HTML page.
     * Since ver. 1.3.
     */
    public static final String Allowed_event_display_message = "\tALLOWED EVENT: ";
    /**
     * For trace and visualization purposes: this class variable is used by the
     * verbose mode. It is also used by
     * <I>PauWare view</I> tool to control what is displayed on the HTML page.
     * Since ver. 1.3.
     */
    public static final String Exit_action_display_message = "\tEXIT ACTION: ";
    /**
     * For trace and visualization purposes: this class variable is used by the
     * verbose mode. It is also used by
     * <I>PauWare view</I> tool to control what is displayed on the HTML page.
     * Since ver. 1.3.
     */
    public static final String Disactivated_state_display_message = "\tDISACTIVATED STATE: ";
    /**
     * For trace and visualization purposes: this class variable is used by the
     * verbose mode. It is also used by
     * <I>PauWare view</I> tool to control what is displayed on the HTML page.
     * Since ver. 1.3.
     */
    public static final String Entry_action_display_message = "\tENTRY ACTION: ";
    /**
     * For trace and visualization purposes: this class variable is used by the
     * verbose mode. It is also used by
     * <I>PauWare view</I> tool to control what is displayed on the HTML page.
     * Since ver. 1.3.
     */
    public static final String Do_activity_display_message = "\tDO ACTIVITY: ";
    /**
     * For trace and visualization purposes: this class variable is used by the
     * verbose mode. It is also used by
     * <I>PauWare view</I> tool to control what is displayed on the HTML page.
     * Since ver. 1.3.
     */
    public static final String Activated_state_display_message = "\tACTIVATED STATE: ";
    /**
     * For visualization purposes only: this class variable is used by the
     * <I>PauWare view</I> tool to clean up unacceptable characters. Since ver.
     * 1.3.
     */
    public static final char Neutral_character = '_';
    /**
     * For visualization purposes only: this class variable is used for breaking
     * into pieces textual views, which are later processed by the
     * <I>PauWare view</I> tool. Since ver. 1.3.
     */
    public static final String Textual_view_subject_separator = "|";

    /**
     * For visualization purposes only: this class method is used by the
     * <I>PauWare view</I> tool to clean up unacceptable characters. Since ver.
     * 1.3.
     */
    public static String Clean_up(String name) {
        for (int i = 0; i < name.length(); i++) {
            if (!((name.charAt(i) >= 'a' && name.charAt(i) <= 'z') || (name.charAt(i) >= 'A' && name.charAt(i) <= 'Z') || (name.charAt(i) >= '0' && name.charAt(i) <= '9'))) {
                name = name.replace(name.charAt(i), Neutral_character);
            }
        }
        return name;
        /**
         * Java SE version
         */
//        return name.replaceAll("[^a-zA-Z0-9]", String.valueOf(Neutral_character));
        /**
         * End of Java SE version
         */
    }

    /**
     * For visualization purposes only: this recursive method is internally
     * called by the {@link AbstractStatechart_monitor#to_PlantUML()} method to
     * compute the state views of the state machine. Since ver. 1.3.
     */
    protected String compute_state_views(Hashtable transition_views) {
        String state_name = Clean_up(_name);
        String state_view = "";
        String history_view = "";
        String input_state_view = "";
        String output_state_view = "";
        if (!AbstractStatechart.Pseudo_state.equals(_name)) {
            if (is_deep_history()) {
                history_view = "note left of " + state_name + " : " + "H*" + "\n";
            } else {
                if (is_shallow_history()) {
                    history_view = "note left of " + state_name + " : " + "H" + "\n";
                }
            }
            if (isInputState()) {
                /**
                 * Java SE
                 */
//                assert (!isOutputState());
                /**
                 * End of Java SE
                 */
                input_state_view += "[*] --> " + state_name + "\n";
            }
            if (isOutputState()) {
                /**
                 * Java SE
                 */
//                assert (!isInputState());
                /**
                 * End of Java SE
                 */
                output_state_view += state_name + " --> [*]\n";
            }
            String allowed_events_label = get_allowed_events_label();
            if (allowed_events_label != null && !allowed_events_label.equals("")) {
                String[] allowed_events_labels = _Split(allowed_events_label);
                /**
                 * Java SE
                 */
//                String[] allowed_events_labels = allowed_events_label.split("\n");
                /**
                 * End of Java SE
                 */
                for (int i = 0; i < allowed_events_labels.length; i++) {
                    state_view += state_name + " : " + allowed_events_labels[i] + "\n";
                }
            }
            String invariant = get_invariant_label();
            if (invariant != null) {
                state_view += state_name + " : " + invariant + "\n";
            }
            String entries = get_entries_label();
            if (entries != null && !entries.equals("")) {
                String[] entries_labels = _Split(entries);
                /**
                 * Java SE
                 */
//                String[] entries_labels = entries.split("\n");
                /**
                 * End of Java SE
                 */
                for (int i = 0; i < entries_labels.length; i++) {
                    state_view += state_name + " : " + entries_labels[i] + "\n";
                }
            }
            String do_activity = get_do_label();
            if (do_activity != null) {
                state_view += state_name + " : " + do_activity + "\n";
            }
            String exits = get_exits_label();
            if (exits != null && !exits.equals("")) {
                String[] exits_labels = _Split(exits);
                /**
                 * Java SE
                 */
//                String[] exits_labels = exits.split("\n");
                /**
                 * End of Java SE
                 */
                for (int i = 0; i < exits_labels.length; i++) {
                    state_view += state_name + " : " + exits_labels[i] + "\n";
                }
            }
        }
        String state_view_prefix = history_view + input_state_view + output_state_view;
        if (leaf()) {
            /**
             * Java SE
             */
//            assert (!transition_views.containsKey(this));
            /**
             * End of Java SE
             */
            state_view = "state " + state_name + "\n" + state_view_prefix + "\n" + state_view;
        } else {
            String transition_view = transition_views.containsKey(this) ? (String) transition_views.get(this) : "";
            String left = _left.compute_state_views(transition_views);
            String right = _right.compute_state_views(transition_views);
            if (AbstractStatechart.Pseudo_state.equals(_name)) {
                if (_left.isXorWith(_right)) {
                    state_view += left + right + transition_view;
                } else {
                    /**
                     * Java SE
                     */
//                    assert (_left.isAndWith(_right));
                    /**
                     * End of Java SE
                     */
                    state_view += left + "--\n" + right + transition_view;
                }
            } else {
                if (_left.isXorWith(_right)) {
                    state_view = state_view_prefix + "state " + state_name + " {\n" + state_view + left + right + transition_view + "}\n";
                } else {
                    /**
                     * Java SE
                     */
//                    assert (_left.isAndWith(_right));
                    /**
                     * End of Java SE
                     */
// Common case:
                    if (!AbstractStatechart.Pseudo_state.equals(_left._name) && !AbstractStatechart.Pseudo_state.equals(_right._name)) {
                        state_view = state_view_prefix + "state " + state_name + " {\n" + state_view + left + "--\n" + right + transition_view + "}\n";
                    }
// Case of single nesting (see 'Single_nesting' NetBeans project):
                    if (AbstractStatechart.Pseudo_state.equals(_left._name) && !AbstractStatechart.Pseudo_state.equals(_right._name)) {
                        state_view = state_view_prefix + "state " + state_name + " {\n" + state_view + right + transition_view + "}\n";
                    }
// Dual case of single nesting (see 'Single_nesting' NetBeans project):
                    if (!AbstractStatechart.Pseudo_state.equals(_left._name) && AbstractStatechart.Pseudo_state.equals(_right._name)) {
                        state_view = state_view_prefix + "state " + state_name + " {\n" + state_view + left + transition_view + "}\n";
                    }
// 'Transaction' statechart in 'Banking_system' NetBeans project):
                    if (AbstractStatechart.Pseudo_state.equals(_left._name) && AbstractStatechart.Pseudo_state.equals(_right._name)) {
                        state_view = state_view_prefix + "state " + state_name + " {\n" + state_view + left + "--\n" + right + transition_view + "}\n";
                    }
                }
            }
        }
        return state_view;
    }

    /**
     * For visualization purposes only: Java ME utility method to replace
     * <CODE>s.split("\n")</CODE>. Since ver. 1.3.
     */
    private static String[] _Split(String s) {
        if (s == null) {
            return new String[0];
        }
        String[] pieces = new String[]{s};
        String[] temp;
        for (int i = 0, index = pieces[i].indexOf("\n"); index != -1; i++, index = pieces[i].indexOf("\n")) {
            temp = new String[pieces.length + 1];
            System.arraycopy(pieces, 0, temp, 0, pieces.length);
            pieces = temp;
            pieces[i + 1] = pieces[i].substring(index + 1);
            pieces[i] = pieces[i].substring(0, index);
        }
        if (pieces[pieces.length - 1].equals("")) {
            temp = new String[pieces.length - 1];
            System.arraycopy(pieces, 0, temp, 0, pieces.length - 1);
            pieces = temp;
        }
        return pieces;
    }
}
