/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Core;

import com.pauware.pauware_engine._Exception.Statechart_exception;

/**
 * This interface is a management utility, which can be implemented by a
 * software component so that it can be accessed by means of Java Management
 * eXtensions (JMX) in particular.
 * <p>
 * Compatibility: <I>PauWare Java EE</I>
 * (Java SE/Java EE) and <I>PauWare Java ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public interface Manageable_base {

    /**
     * This method returns the current state of a software component.
     * <p>
     * Its typical implementation is as follows:
     * <CODE>return _my_state_machine.async_current_state();</CODE> where the
     * type of the <CODE>_my_state_machine</CODE> field in the component is
     * {@link AbstractStatechart_monitor}. While
     * {@link Manageable_base#current_state()} computes the real-time state of
     * <CODE>_my_state_machine</CODE> with a risk of unreliable result (if
     * called when a run-to-completion cycle is in progress), this method is
     * more reliable in the sense that it returns the recorded state in the very
     * last run-to-completion cycle.
     */
    String async_current_state();

    /**
     * This method returns the current (real-time) state of a software
     * component.
     * <p>
     * Its typical implementation is as follows:
     * <CODE>return _my_state_machine.current_state();</CODE> where the type of
     * the <CODE>_my_state_machine</CODE> field in the component is
     * {@link AbstractStatechart_monitor}.
     *
     * @see Manageable_base#async_current_state()
     */
    String current_state();

    /**
     * This method detects if a given state of a software component is active.
     * <p>
     * Its typical implementation is as follows:
     * <CODE>return _my_state_machine.in_state("A state");</CODE> where the type
     * of the <CODE>_my_state_machine</CODE> field in the component is
     * {@link AbstractStatechart_monitor}.
     */
    boolean in_state(String name);

    /**
     * This method returns the preferred name of a software component to be
     * managed by a naming service.
     * <p>
     * Its typical implementation is as follows:
     * <CODE>return _my_state_machine.name();</CODE> where the type of the
     * <CODE>_my_state_machine</CODE> field in the component is
     * {@link AbstractStatechart_monitor}.
     */
    String name();

    /**
     * This method returns the last result of a run-to-completion cycle plus the
     * current state of a software component.
     * <p>
     * Its typical implementation is as follows:
     * <CODE>return _my_state_machine.verbose();</CODE> where the type of the
     * <CODE>_my_state_machine</CODE> field in the component is
     * {@link AbstractStatechart_monitor}.
     */
    String verbose();
}
