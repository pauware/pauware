/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Core;

/**
 * This interface is a composition utility, which has to be implemented by a
 * software component for state machine-based composition. <p> Compatibility:
 * <I>PauWare Java EE</I> (Java SE/Java EE) and <I>PauWare Java ME</I> (Java
 * SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public interface Composable {

    /**
     * This method returns the internal state machine of a software component.
     * <p> Its typical implementation is as follows:
     * <CODE>return _my_state_machine;</CODE> where the type of the
     * <CODE>_my_state_machine</CODE> field in the component is
     * {@link AbstractStatechart_monitor}.
     */
    AbstractStatechart_monitor state_machine();
}
