/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Core;

import com.pauware.pauware_engine._Exception.Statechart_exception;

/**
 * This abstract class represents the general notion of <I>Action</I> in UML.
 * Instances of this class are executed when entering states (<I>entry/</I>
 * notation), exiting states (<I>exit/</I> notation) and at event processing
 * time (<I>event[guard]/action(s)</I> notation). . Use of this class occurs
 * through the following subclasses:
 * {@link com.pauware.pauware_engine._Java_EE.Action}, {@link com.pauware.pauware_engine._Java_EE.SendSignalAction}
 * (<I>PauWare Java EE</I> - Java SE/Java EE) or
 * {@link com.pauware.pauware_engine._Java_ME.Java_MEAction}, {@link com.pauware.pauware_engine._Java_ME.Java_MESendSignalAction}
 * (<I>PauWare Java ME</I> - Java SE/Java ME). It is instantiated within the
 * {@link com.pauware.pauware_engine._Core.AbstractStatechart_monitor#fires(String,AbstractStatechart,AbstractStatechart,Object,String,Object[],Object,String,Object[],byte)}
 * method. Instances of this class are executed by means of the
 * {@link com.pauware.pauware_engine._Core.AbstractAction#execute()} method
 * during a run-to-completion cycle
 * ({@link com.pauware.pauware_engine._Core.AbstractStatechart_monitor#run_to_completion(String,boolean)}
 * method).
 * <p>
 * Compatibility: <I>PauWare Java EE</I> (Java SE/Java EE) and
 * <I>PauWare Java ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
abstract public class AbstractAction {

    /**
     * This class variable is the symbol of an action, which sends a signal (an
     * event).
     */
    public static final char SendSignalAction_symbol = '^';

    /**
     * The default value is the <CODE>"no result" String</CODE> instance if
     * there is no execution.
     */
    public static final String Pseudo_result = "no result";
    /**
     * This field represents the object in charge of executing the action.
     */
    protected Object _object;
    /**
     * This field represents the action itself.
     */
    protected String _action;
    /**
     * This field represents the action's arguments.
     */
    protected Object[] _args;
    /**
     * This field represents the action's result, if already executed.
     *
     * @see AbstractAction#Pseudo_result
     */
    protected Object _result;

    /**
     * This constructor is called by the
     * {@link AbstractStatechart_monitor#fires(String,AbstractStatechart,AbstractStatechart,Object,String,Object[],Object,String,Object[],byte)}
     * method.
     *
     * @param object The object executing the action; Java arrays are supported
     * in order to enable multicasting (<CODE>null</CODE> means no effect).
     * @param action The executed action (<CODE>null</CODE> means no effect).
     * @param args The action's arguments (<CODE>null</CODE> means no arguments)
     */
    protected AbstractAction(Object object, String action, Object[] args) {
        _object = object;
        _action = action;
        set_args(args);
        _result = Pseudo_result;
    }

    /**
     * This method is used when an <I>Action</I> is put in a hashtable as a key.
     */
    public boolean equals(Object action) {
        if (this == action) {
            return true;
        }
        if (action instanceof AbstractAction) {
            return (_object == null ? ((AbstractAction) action)._object == null : _object.equals(((AbstractAction) action)._object)) && (_action == null ? ((AbstractAction) action)._action == null : _action.equals(((AbstractAction) action)._action)) && (_args == null ? ((AbstractAction) action)._args == null : compare_args(((AbstractAction) action)._args));
        }
        return false;
    }

    /**
     * This method is used when an <I>Action</I> is put in a hashtable as a key.
     */
    public int hashCode() {
        return (_object == null ? 0 : _object.hashCode()) ^ (_action == null ? 0 : _action.hashCode()) ^ (_args == null ? 0 : hashCode_args());
    }

    /**
     * Implementation of this method depends upon the chosen platform.
     *
     * @throws Statechart_exception An encapsulation of any Java problem that
     * results from executing the action
     */
    abstract protected void execute() throws Statechart_exception;

    /**
     * This method allows the possibility of waiting for actions to complete.
     */
    abstract protected void wait_for_completion() throws Statechart_exception;

    /**
     * This method is called by the {@link AbstractAction#equals(Object)}
     * method.
     */
    protected boolean compare_args(Object[] args) {
        if (args == null) {
            return _args == null ? true : false;
        }
        if (args.length != _args.length) {
            return false;
        }
        for (int i = 0; i < args.length; i++) {
            if (args[i] != null && _args[i] != null && !(_args[i].getClass().isInstance(args[i]))) {
                return false;
            }
        }
        return true;
    }

    /**
     * This method is called by the {@link AbstractAction#hashCode()} method.
     * <b>Caution:</b>: actions are known to be similar when the type and the
     * number of their arguments are the same. The identities of their arguments
     * are NOT used at comparison time. In this perspective, this method
     * guarantees that <CODE>a1.equals(a2)</CODE> implies
     * <CODE>a1.hashCode() == a2.hashCode()</CODE>.
     */
    public int hashCode_args() {
        // pre-condition: _args != null
        int hashCode = 0;
        for (int i = 0; i < _args.length; i++) {
            hashCode ^= (_args[i] == null ? 0 : _args[i].getClass().hashCode());
        }
        return hashCode;
    }

    /**
     * This method is called by the
     * {@link AbstractAction#AbstractAction(Object,String,Object[])}
     * constructor.
     */
    protected void set_args(Object[] args) {
        if (args == null) {
            _args = null;
        } else {
            _args = new Object[args.length];
            for (int i = 0; i < _args.length; i++) {
                _args[i] = args[i];
            }
        }
    }

    /**
     * This method is called by the {@link AbstractAction#verbose()} method.
     */
    protected String printable_action() {
        if (_action == null) {
            return "null";
        }
        return _action;
    }

    /**
     * This method is called by the {@link AbstractAction#verbose()} method.
     */
    protected String printable_object() {
        if (_object == null) {
            return "null";
        }
        return _object.getClass().getName().substring(_object.getClass().getName().lastIndexOf('.') + 1);
        /**
         * Java SE
         */
//        return _object.getClass().getSimpleName();
        /**
         * End of Java SE
         */
    }

    /**
     * This method is called by the {@link AbstractAction#verbose()} method.
     */
    protected String printable_result() {
        if (_result == null) {
            return "null";
        }
        if (_result.toString().indexOf('@') == -1) {
            return _result.toString();
        } else {
            return _result.getClass().getName().substring(_result.getClass().getName().lastIndexOf('.') + 1) + _result.toString().substring(_result.toString().indexOf('@'));
        }
        /**
         * Java SE
         */
//        return _result.toString().replace(_result.getClass().getName(), _result.getClass().getSimpleName());
        /**
         * End of Java SE
         */
    }

    /**
     * This method returns the detailed result of the action's execution, or the
     * content of the {@link AbstractAction#Pseudo_result} class variable in
     * case of the {@link AbstractAction#execute()} method has not yet been run.
     */
    protected String verbose() {
        return to_UML() + AbstractStatechart.Textual_view_subject_separator + "result: " + printable_result();
    }

    /**
     * This method returns the action in the form of a UML-compliant string.
     */
    public String to_UML() {
        String label = "";
        label += printable_object() + '.' + printable_action();
        if (_args != null) {
            label += '(';
            for (int i = 0; i < _args.length; i++) {
                if (_args[i] == null) {
                    label += Object.class.getName().substring(Object.class.getName().lastIndexOf('.') + 1) + ',';
                } else {
                    label += _args[i].getClass().getName().substring(_args[i].getClass().getName().lastIndexOf('.') + 1) + ',';
                    /**
                     * Java SE
                     */
//                label += _args[i].getClass().getSimpleName() + ',';
                    /**
                     * End of Java SE
                     */
                }
            }
            label = label.substring(0, label.lastIndexOf(',')) + ')';
        }
        return label;
    }

    public Object getObject() {
        return _object;
    }

    public String getAction() {
        return _action;
    }

    public Object[] getArgs() {
        return _args;
    }

    public Object getResult() {
        return _result;
    }
    
    
}
