/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pauware.pauware_engine._Core;

import com.pauware.pauware_engine._Exception.*;
import com.pauware.pauware_engine._Java_ME.*;

/**
 *
 * @author ercar
 */
public class PauWareManagerME extends PauWareManager {
    
    @Override
    public AbstractStatechart createStatechart(String name) {
        return new Java_MEStatechart(name);
    }
    
    @Override
    public AbstractStatechart_monitor createStateMachine(AbstractStatechart state, String name) throws Statechart_exception {
       return  new Java_MEStatechart_monitor(state, name);    
    }

    @Override
    public AbstractStatechart_monitor createStateMachine(AbstractStatechart state, String name, boolean show_on_system_out) throws Statechart_exception {
        return new Java_MEStatechart_monitor(state, name, show_on_system_out);
    }

    @Override
    public AbstractStatechart_monitor createStateMachine(AbstractStatechart state, String name, boolean show_on_system_out, AbstractStatechart_monitor_listener listener) throws Statechart_exception {
        return new Java_MEStatechart_monitor(state, name, show_on_system_out, listener);
    }
 
    @Override
    public Transition creationTransition(String event, AbstractStatechart from, AbstractStatechart to) {
        return new Transition(event, from, to);
    }

    /*
    @Override
    public AbstractAction createAction(Object actionObject, String actionMethodName) {
        return new Action(actionObject, actionMethodName, null);
    }

    @Override
    public AbstractAction createAction(Object actionObject, String actionMethodName, Object... actionParams) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AbstractBooleanAction createBooleanAction(Object actionObject, String actionMethodName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AbstractBooleanAction createBooleanAction(Object actionObject, String actionMethodName, Object... actionParams) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
*/
}
