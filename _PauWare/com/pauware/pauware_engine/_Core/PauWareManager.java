/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pauware.pauware_engine._Core;

import com.pauware.pauware_engine._Exception.Statechart_exception;

/**
 *
 * @author lea
 */
public abstract class PauWareManager implements IPauWare {
    
    public static Observable trace = null;

    @Override
    public void setTrace(Observable t) {
        trace = t;
    }
    
    public static Boolean isTraceable() {
        return trace != null;
    }
    
}
