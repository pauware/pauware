/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Core;

import com.pauware.pauware_engine._Exception.Statechart_exception;

/**
 * This interface is a utility that can be implemented by a software component
 * which wants to have <I>timer services</I> at its disposal. However,
 * inheriting from
 * {@link com.pauware.pauware_engine._Core.AbstractTimer_monitor} or
 * {@link com.pauware.pauware_engine._Java_EE.Timer_monitor} is preferable
 * because these two classes provide default implementations.
 * <p>
 * Compatibility:
 * <I>PauWare Java EE</I> (Java SE/Java EE) and <I>PauWare Java ME</I> (Java
 * SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public interface Timed_component {

    /**
     * This method is called when <CODE>delay</CODE> is elapsed.
     * <p>
     * A software component receiving a
     * <I>time-out</I> may check the delay and/or the context in which, it asks
     * for <I>timer services</I>. Normally, the content of this method starts
     * with several transitions (see the
     * {@link com.pauware.pauware_engine._Core.AbstractStatechart_monitor#fires(String,AbstractStatechart,AbstractStatechart,Object,String,Object[],Object,String,Object[],byte)}
     * method) and ends with a run-to-completion cycle, namely
     * <CODE>_my_state_machine.run_to_completion("time_out");</CODE> where the
     * type of the <CODE>_my_state_machine</CODE> field in the component is
     * {@link AbstractStatechart_monitor}.
     */
    void time_out(long delay, AbstractStatechart context) throws Statechart_exception;

    /**
     * This method is called when <I>timer services</I> raise problems.
     * <p>
     * A
     * software component receiving a <I>time-out error</I> may know the reason
     * why the requested <I>timer services</I> fail (<CODE>se</CODE> parameter).
     * The content of this method may be left empty. The requested
     * <I>timer services</I> that are causing problems are indeed automatically
     * canceled.
     */
    void time_out_error(Statechart_exception se) throws Statechart_exception;
}
