package com.pauware.pauware_engine._Core;

/**
 * This interface defines the abstract operations for creation the state machine elements. It is a factory independent of 
 * the concrete used Java platform (Java EE or Java ME). 
 * It is implemented by the PauWare managers returned by the builder of {@link PauWareFactory}.
 * @author Eric Cariou
 */
import com.pauware.pauware_engine._Exception.Statechart_exception;

public interface IPauWare {
    
    /**
     * Create a state 
     * @param name the name of the state
     * @return the created state
     */
    public AbstractStatechart createStatechart(String name);
    
    /**
     * Create a state machine monitor
     * @param state the state defining the state machine
     * @param name the name of the state machine
     * @return the state machine
     * @throws Statechart_exception in case of problem 
     */
    public AbstractStatechart_monitor createStateMachine(AbstractStatechart state, String name) throws Statechart_exception;
   
    /**
     * Create a state machine monitor
     * @param state the state defining the state machine
     * @param name the name of the state machine
     * @param show_on_system_out if an execution trace has to be written on the console
     * @return the state machine
     * @throws Statechart_exception in case of problem 
     */ 
    public AbstractStatechart_monitor createStateMachine(AbstractStatechart state, String name, boolean show_on_system_out) throws Statechart_exception;
    
    /**
     * Create a state machine monitor
     * @param state the state defining the state machine
     * @param name the name of the state machine
     * @param show_on_system_out if an execution trace has to be written on the console
     * @param listener the PauWare listener of the state machine. Please note that this observer is not
     * an implementation of the Observable interface but is an "older" listener used for instance to display
     * the state machine under execution using Plant UML
     * @return the state machine
     * @throws Statechart_exception in case of problem
     */
    public AbstractStatechart_monitor createStateMachine(AbstractStatechart state, String name, boolean show_on_system_out, AbstractStatechart_monitor_listener listener) throws Statechart_exception;
    
    /**
     * Create a transition. It creates a basic transition that can afterwards be extended with the addition of a business operation, a guard...
     * @param event the event of the transtion
     * @param from the source state of the transition
     * @param to the target state of the transition
     * @return the created transition
     */
    public Transition creationTransition(String event, AbstractStatechart from, AbstractStatechart to);
  
    /**
     * Add a monitor to the state machine
     * @param t the monitor instance
     */
    public void setTrace(Observable t);
    
    /*
    public AbstractAction createAction(Object actionObject, String actionMethodName);
    
    public AbstractAction createAction(Object actionObject, String actionMethodName, Object... actionParams);
    
    public AbstractBooleanAction createBooleanAction(Object actionObject, String actionMethodName);
    
    public AbstractBooleanAction createBooleanAction(Object actionObject, String actionMethodName, Object... actionParams);
    */
}
