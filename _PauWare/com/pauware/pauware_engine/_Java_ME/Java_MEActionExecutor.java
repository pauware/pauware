/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Java_ME;

/**
 * This interface is a utility for bypassing the reflection mechanism of Java.
 * <p>
 * Compatibility: <I>PauWare Java ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public interface Java_MEActionExecutor extends Executor {

    /**
     * This method is called by a state machine during a run-to-completion
     * cycle. A software component which has such a state machine in its inside
     * may delegate the implementation of this interface to another object type
     * (<I>e.g.</I>, a user-defined <CODE>Delegator</CODE> Java class having a
     * constructor <b>without</b>
     * arguments). In such a case, this object type must be provided to the
     * {@link com.pauware.pauware_engine._Core.AbstractStatechart_monitor#fires(String,AbstractStatechart,AbstractStatechart,Object,String,Object[],Object,String,Object[],byte)}
     * method (or another variant of this method) as illustrated:
     * <p>
     * <CODE>
     * _Programmable_thermostat_state_machine.fires("temp_down", _Run, _Run,
     * this, "target_temperature_greaterThan_Min", null,
     * _target_temperature, "com.FranckBarbier.Java._Programmable_thermostat_MODEL._Programmable_thermostat.Programmable_thermostatActionExecutor.decrement");
     * </CODE>
     * <p>
     * As a result, actions and guards declared in transitions (as well as
     * actions when entering or exiting states) must be activated by this
     * method, as follows:
     * <p>
     * <CODE>
     * <br>public class Programmable_thermostatActionExecutor implements
     * Java_MEActionExecutor {
     * <br> // constructor without arguments is mandatory
     * <br> public Object execute(Object object,String action,Object[] args)
     * throws Throwable {
     * <br> Object result = null;
     * <br> ...
     * <br> if(action != null &amp;&amp; action.equals("decrement")) result =
     * ((Temperature)object).decrement();
     * <br> // 'args' is not used here because the 'decrement' action is
     * supposed to have no parameters
     * <br> ...
     * <br> return result;
     * <br> }
     * <br>}
     * </CODE>
     */
    Object execute(Object object, String action, Object[] args) throws Throwable;
}
