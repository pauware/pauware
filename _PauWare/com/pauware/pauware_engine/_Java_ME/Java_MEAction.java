/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Java_ME;

import com.pauware.pauware_engine._Core.AbstractAction;
import com.pauware.pauware_engine._Exception.Statechart_exception;

/**
 * This concrete class represents the general notion of <I>Action</I> in UML.
 * <p>
 * Compatibility: <I>PauWare Java ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public class Java_MEAction extends AbstractAction {

    /**
     * This object acts as an execution "delegator" resulting from the
     * constraint that reflection is not permitted in the Java ME platform. In
     * most cases, this object is the software component which owns a state
     * machine and requires an action to be executed during a run-to-completion
     * cycle of this state machine. In such a case, this component implements
     * the {@link Java_MEExecutor} interface.
     */
    protected Executor _executor = null;

    /**
     * @see AbstractAction#AbstractAction(Object,String,Object[])
     */
    protected Java_MEAction(Object object, String action, Object[] args) {
        super(object, action, args);
        try {
            if (_object != null) {
                _executor = (Java_MEExecutor) _object;
            }
        } catch (ClassCastException cce) {
            if (_action != null) {
                try {
                    _executor = (Java_MEActionExecutor) Class.forName(_action.substring(0, _action.lastIndexOf('.'))).newInstance();
                    _action = _action.substring(_action.lastIndexOf('.') + 1, _action.length());
                } catch (Throwable t) {
                    _result = _object.toString() + "." + _action + " failed: " + t.toString();
                }
            }
        }
    }

    /**
     * This method is called during a run-to-completion cycle and executes the
     * action instance by using a <b>{@link Executor}</b> object; Actions may be
     * re-executed.
     * <p>
     * This method is called automatically, and more precisely, at an
     * appropriate moment with respect to the functioning of a state machine.
     * All kinds of probable exceptions including Java <CODE>Error</CODE>
     * instances and runtime exceptions are caught by this method. The computed
     * result is assigned to the <CODE>_result</CODE> variable and is made
     * readable through the {@link AbstractAction#verbose()} method.
     */
    protected void execute() throws Statechart_exception {
        if (_executor == null) {
            throw new Statechart_exception(_result.toString());
        }
        try {
            _result = ((Java_MEExecutor) _executor).execute(_action, _args);
        } catch (ClassCastException cce) {
            try {
                _result = ((Java_MEActionExecutor) _executor).execute(_object, _action, _args);
            } catch (Throwable t) {
                _result = _executor.toString() + "." + _action + " failed: " + t.toString();
                throw new Statechart_exception(_result.toString());
            }
        } catch (Throwable t) {
            _result = _executor.toString() + "." + _action + " failed: " + t.toString();
            throw new Statechart_exception(_result.toString());
        }
    }

    /**
     * This method allows the possibility of waiting for actions to complete.
     * Since UML actions are recognized as instantaneous, this method is left
     * empty.
     */
    protected void wait_for_completion() throws Statechart_exception {
    }
}
