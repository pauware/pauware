/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Java_ME;

/**
 * This interface is a utility for bypassing the reflection mechanism of Java.
 * <p>
 * Compatibility: <I>PauWare Java ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public interface Java_MEExecutor extends Executor {

    /**
     * This method is called by a state machine during a run-to-completion
     * cycle. A software component that has such a state machine in its inside
     * may implement <CODE>Java_MEExecutor</CODE>. As a result, actions and
     * guards declared in transitions (see:
     * {@link com.pauware.pauware_engine._Core.AbstractStatechart_monitor#fires(String,AbstractStatechart,AbstractStatechart,Object,String,Object[],Object,String,Object[],byte)}
     * (as well as actions when entering or exiting states) must be activated
     * within this method, as follows:
     * <p>
     * <CODE>
     * <br>Object result = null;
     * <br>...
     * <br>if(action != null &amp;&amp; action.equals("my_action")) result =
     * this.my_action((String)args[0]);
     * <br>// 'args[0]' is used here for illustration purposes only
     * <br>...
     * <br>return result;
     * </CODE>
     */
    Object execute(String action, Object[] args) throws Throwable;
}
