/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Java_ME;

import com.pauware.pauware_engine._Core.*;

/**
 * This concrete class represents the general notion of <I>State</I> in UML.
 * <p>
 * Compatibility: <I>PauWare Java ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public class Java_MEStatechart extends AbstractStatechart {

    /**
     * @see AbstractStatechart#AbstractStatechart(String)
     */
    public Java_MEStatechart(String name) {
        super(name);
    }

    /**
     * This method creates and returns an action instance that is compatible
     * with the chosen platform; This is an instance of {@link Java_MEAction} if
     * the <CODE>reentrance_mode</CODE> parameter is equal to
     * <CODE>AbstractStatechart.No_reentrance</CODE>; Otherwise, it is an
     * instance of {@link Java_MESendSignalAction}.
     */
    protected AbstractAction action(Object object, String action, Object[] args, byte reentrance_mode) {
        switch (reentrance_mode) {
            case No_reentrance:
                return new Java_MEAction(object, action, args);
            case Reentrance:
                return new Java_MESendSignalAction(object, action, args);
            default:
                return new Java_MESendSignalAction(object, action, args);
        }
    }

    /**
     * This method creates and returns an activity instance that is compatible
     * with the chosen platform; This is an instance of
     * {@link Java_MEDo_activity}.
     */
    protected AbstractAction activity(Object object, String action, Object[] args) {
        return new Java_MEDo_activity(object, action, args);
    }

    /**
     * This method creates and returns a guard instance that is compatible with
     * the chosen platform; This is an instance of {@link Java_MEGuard}.
     */
    protected AbstractGuard guard(Object guard_object, String guard_action, Object[] guard_args) {
        return new Java_MEGuard(guard_object, guard_action, guard_args);
    }

    /**
     * This method creates and returns a superstate instance that is compatible
     * with the chosen platform; This is an instance of
     * {@link Java_MEStatechart}.
     */
    protected AbstractStatechart createFather() {
        return new Java_MEStatechart(null);
    }
}
