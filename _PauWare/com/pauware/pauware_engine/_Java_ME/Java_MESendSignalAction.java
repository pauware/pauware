/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Java_ME;

import com.pauware.pauware_engine._Exception.Statechart_exception;

/**
 * This concrete class represents the general notion of <I>Action</I> in UML,
 * which consists in sending a signal.
 * <p>
 * Compatibility: <I>PauWare Java ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public class Java_MESendSignalAction extends Java_MEAction implements Runnable {

    /**
     * This field refers to the action (if any) being executed; it corresponds
     * to sending a signal.
     */
    protected Thread _thread = null;

    /**
     * @see Java_MEAction#Java_MEAction(Object,String,Object[])
     */
    protected Java_MESendSignalAction(Object object, String action, Object[] args) {
        super(object, action, args);
    }

    /**
     * This method executes an action in a standalone thread of control. Such an
     * action is the self-sending of an event
     *
     * @see Java_MEAction#execute()
     */
    protected void execute() throws Statechart_exception {
        _thread = new Thread(this, _executor.toString() + '.' + _action);
        /**
         * Modified on Nov. 2013
         */
        /**
         * The line of code below has been added because models are often
         * ill-formed. Indeed, modelers wrongly believe that a 's' event sent
         * internally by 'a', is processed BEFORE 'b', which is the event
         * following 'a' in a given state machine sequence. Java tends to
         * process 's' too late. So, one may improve the situation, given 's' a
         * higher processing priority, but the best amounts to building more
         * robust models!
         */
        _thread.setPriority(Thread.MAX_PRIORITY);
        /**
         * End of modification
         */
        try {
            _thread.start();
        } catch (IllegalThreadStateException itse) {
            _result = _thread.getName() + " failed: " + itse.toString();
            throw new Statechart_exception(_result.toString());
        }
    }

    /**
     * This method calls the {@link Java_MEAction#execute()} method in order to
     * synchronize (defer) the action execution with respect to any
     * run-to-completion cycle that is in progress.
     */
    public void run() {
        try {
            super.execute();
        } catch (Statechart_exception se) {
            /**
             * 'se' is already assigned to '_result'
             */
            se.printStackTrace();
        }
    }

    /**
     * This method returns the action in the form of a UML-compliant string.
     */
    public String to_UML() {
        String result = super.to_UML();
        return result != null ? SendSignalAction_symbol + result : null;
    }
}
