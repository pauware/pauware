/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Java_ME;

/**
 * This interface is a utility which creates a common supertype for the
 * {@link Java_MEExecutor} and {@link Java_MEActionExecutor} interfaces. It is
 * empty.
 * <p>
 * Compatibility: <I>Executor</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public interface Executor {
}
