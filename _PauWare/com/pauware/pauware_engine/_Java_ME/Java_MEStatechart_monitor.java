/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Java_ME;

import com.pauware.pauware_engine._Core.AbstractAction;
import com.pauware.pauware_engine._Core.AbstractGuard;
import com.pauware.pauware_engine._Core.AbstractStatechart;
import com.pauware.pauware_engine._Core.AbstractStatechart_monitor;
import com.pauware.pauware_engine._Core.AbstractStatechart_monitor_listener;
import com.pauware.pauware_engine._Exception.*;

/**
 * This concrete class represents the general notion of <I>State Machine</I> in
 * UML.
 * <p>
 * Compatibility: <I>PauWare Java ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public class Java_MEStatechart_monitor extends AbstractStatechart_monitor {
    // Vertical composition issues: constructor used by createFather()

//    private Java_MEStatechart_monitor() {
//        super();
//    }
    /**
     * @see
     * AbstractStatechart_monitor#AbstractStatechart_monitor(AbstractStatechart,String)
     */
    public Java_MEStatechart_monitor(AbstractStatechart s, String name) throws Statechart_exception {
        super(s, name);
    }

    /**
     * @see
     * AbstractStatechart_monitor#AbstractStatechart_monitor(AbstractStatechart,String,boolean)
     */
    public Java_MEStatechart_monitor(AbstractStatechart s, String name, boolean show_on_system_err) throws Statechart_exception {
        super(s, name, show_on_system_err);
    }

    /**
     * @see
     * AbstractStatechart_monitor#AbstractStatechart_monitor(AbstractStatechart,String,boolean,AbstractStatechart_monitor_listener)
     */
    public Java_MEStatechart_monitor(AbstractStatechart s, String name, boolean show_on_system_err, AbstractStatechart_monitor_listener listener) throws Statechart_exception {
        super(s, name, show_on_system_err, listener);
    }

    /**
     * This method raises an instance of <CODE>RuntimeException</CODE>. This is
     * because <CODE>AbstractStatechart_monitor</CODE> instances are "root"
     * states and thus may not be composed in larger state machines. However,
     * this method is intended to be replaced as soon as the notion of "vertical
     * composition" will be implemented within <I>PauWare</I>.
     */
    protected AbstractStatechart createFather() {
        throw new RuntimeException("Attempting to creating a 'father' state for a root state");
        // Vertical composition issues:
        // return new Java_MEStatechart_monitor();
        // memory burden, too many monitors in the state hierarchy
    }

    /**
     * This method creates and returns an action instance that is compatible
     * with the chosen platform; This is an instance of {@link Java_MEAction} if
     * the <CODE>reentrance_mode</CODE> parameter is equal to
     * <CODE>AbstractStatechart.No_reentrance</CODE>; Otherwise, it is an
     * instance of {@link Java_MESendSignalAction}.
     */
    protected AbstractAction action(Object object, String action, Object[] args, byte reentrance_mode) {
        switch (reentrance_mode) {
            case No_reentrance:
                return new Java_MEAction(object, action, args);
            case Reentrance:
                return new Java_MESendSignalAction(object, action, args);
            default:
                return new Java_MESendSignalAction(object, action, args);
        }
    }

    /**
     * This method creates and returns an activity instance that is compatible
     * with the chosen platform; This is an instance of
     * {@link Java_MEDo_activity}.
     */
    protected AbstractAction activity(Object object, String action, Object[] args) {
        return new Java_MEDo_activity(object, action, args);
    }

    /**
     * This method creates and returns a guard instance that is compatible with
     * the chosen platform; This is an instance of {@link Java_MEGuard}.
     */
    protected AbstractGuard guard(Object guard_object, String guard_action, Object[] guard_args) {
        return new Java_MEGuard(guard_object, guard_action, guard_args);
    }
}
