/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Java_ME;

import com.pauware.pauware_engine._Core.AbstractAction;
import com.pauware.pauware_engine._Core.AbstractGuard;

/**
 * This concrete class represents the general notion of <I>Guard</I> in UML.
 * <p>
 * Compatibility: <I>PauWare Java ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.0
 */
public class Java_MEGuard extends AbstractGuard {

    /**
     * @see AbstractGuard#AbstractGuard(Object,String,Object[])
     */
    protected Java_MEGuard(Object guard_object, String guard_action, Object[] guard_args) {
        super(guard_object, guard_action, guard_args);
    }

    /**
     * This method creates and returns an action instance that is compatible
     * with the chosen platform; This is an instance of {@link Java_MEAction}.
     * Note that guards cannot send signals and thus should only evaluate local
     * data. As a result, an instance of the {@link Java_MESendSignalAction}
     * class cannot be returned.
     */
    protected AbstractAction action(Object object, String action, Object[] guard_args) {
        return new Java_MEAction(object, action, guard_args);
    }
}
