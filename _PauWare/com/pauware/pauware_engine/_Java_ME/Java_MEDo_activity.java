/**
 * PauWare engine software (http://www.PauWare.com) InterDeposit Digital Number
 * IDDN.FR.001.360023.000.S.P.2006.000.10000. Use of this software is subject to
 * the restrictions of the LGPL license version 3
 * http://www.gnu.org/licenses/lgpl-3.0.en.html
 */
package com.pauware.pauware_engine._Java_ME;

import com.pauware.pauware_engine._Exception.Statechart_exception;

/**
 * This concrete class represents the general notion of <I>Activity</I> in UML
 * (<I>do/</I> notation).
 * <p>
 * Compatibility: <I>PauWare Java ME</I> (Java SE/Java ME).
 *
 * @author Franck.Barbier@FranckBarbier.com
 * @version 1.3 (February 2015)
 * @since 1.3
 */
public class Java_MEDo_activity extends Java_MESendSignalAction {

    /**
     * @see
     * Java_MESendSignalAction#Java_MESendSignalAction(Object,String,Object[])
     */
    protected Java_MEDo_activity(Object object, String action, Object[] args) {
        super(object, action, args);
    }

    /**
     * This method executes an activity (<I>do/</I> notation) in a standalone
     * thread of control.
     *
     * @see Java_MESendSignalAction#execute()
     */
    protected void execute() throws Statechart_exception {
        wait_for_completion();
        _thread = new Thread(this, _object.toString() + '.' + _action);
        try {
            _thread.start();
        } catch (IllegalThreadStateException itse) {
            _result = _thread.getName() + " failed: " + itse.toString();
            throw new Statechart_exception(_result.toString());
        }
    }

    /**
     * This method indefinitely waits for an activity to complete. Since UML
     * activities (<I>do/</I> notation) last, this method waits for the
     * completion of the immediate prior execution.
     */
    protected void wait_for_completion() throws Statechart_exception {
        if (_thread != null) {
            try {
                _thread.join();
            } catch (InterruptedException ie) {
                _result = _thread.getName() + " failed: " + ie.toString();
                throw new Statechart_exception(_result.toString());
            }
        }
    }
}
