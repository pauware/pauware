package test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Eric
 */
public enum WiperSpeed {

    HIGH("High"),
    NORMAL("Normal"),
    INTERMITTENT3("Intermittent 3"),
    INTERMITTENT2("Intermittent 2"),
    INTERMITTENT1("Intermittent 1"),
    OFF("None");

    private String speed;

    WiperSpeed(String speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return speed;
    }
}
