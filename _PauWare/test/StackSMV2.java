package test;

import com.pauware.pauware_engine._Core.*;
import com.pauware.pauware_engine._Exception.*;
//import com.pauware.pauware_engine._Java_EE.*;
import java.io.IOException;
import java.util.Scanner;

public class StackSMV2 {
    
    // The states of the state machine
    protected AbstractStatechart empty;
    protected AbstractStatechart notEmpty;
    protected AbstractStatechart_monitor stateMachine;
    
    protected Transition pushEmptyNotEmpty;
    protected Transition pushNotEmptyEmpty;
    protected Transition popEmptyNotEmpty;
    protected Transition popNotEmptyEmpty;
  
    
    /**
     * The stack (the business object)
     */
    protected java.util.Stack<String> stack;
    
    /*********************************************
     *               Invariants
     *********************************************/
    
    /**
     * The invariant for the not empty state 
     * @return true if the stack is not empty
     */
    public boolean stackNotEmpty() {
        return ! stack.isEmpty();
    }
    
    /**
     * The invariant for the empty state 
     * @return true if the stack is empty
     */
    public boolean stackEmpty() {
        return stack.isEmpty();
    }
    
     /*********************************************
     *               Guards
     **********************************************/
    
    /**
     * @return true if there is only one element in the stack 
     */
    public boolean onlyOne() {
        System.out.println("** Only one guard call: "+stack.size()+" element(s)");
        return stack.size() == 1;
    }
    /**
     * @return true if there is more than one element in the stack 
     */
    public boolean moreThanOne() {
        System.out.println("** More than one guard call: "+stack.size()+" element(s)");
        return stack.size() > 1;
    }
    
    /*********************************************
     *               Business operations
     *********************************************/
    
    /**
     * For a push on the stack
     * @param value the value to push
     */
    public void actionPush(String value) {
        System.out.println("--> push: "+value);
        stack.push(value);
    }
    
    /**
     * For a pop on the stack
     */
    public void actionPop() {
        String value = stack.pop();
        // Uncomment the following line to pop twice an element
        // It will lead to a violation of the invariants
        // stack.pop();
        System.out.println("--> pop: "+value);
    }
    
    /*********************************************
     *               Events
     *********************************************/
    
    /**
     * The process of the push event
     * @param value the value associated with the event
     * @throws Statechart_transition_based_exception
     * @throws Statechart_exception 
     */
    public void pushEvent(String value) throws Statechart_transition_based_exception, Statechart_exception {
        // The transitions are redefined for putting the value as parameter when calling
        // the "actionPush" method
        
        // t1.setBusinessParams(value);
        // t1.moduifyTransition(sm);
        
        stateMachine.fires("push", empty, notEmpty, true, this, "actionPush", new Object[] { value });
        stateMachine.fires("push", notEmpty, notEmpty, true, this, "actionPush", new Object[] { value });
        stateMachine.run_to_completion("push", AbstractStatechart_monitor.Compute_invariants);
    }
    
    /**
     * The process of the pop event
     * @throws Statechart_exception 
     */
    public void popEvent() throws Statechart_exception {
        stateMachine.run_to_completion("pop", AbstractStatechart_monitor.Compute_invariants);
    }
    
    /**
     * Build and launch the state machine
     * @throws Statechart_exception 
     */
    public void buildStateMachine() throws Statechart_exception, IOException {
        
        IPauWare pw = PauWareFactory.getPauWareManager(JavaPlatform.JavaEE);
        Observable trace = new LTLTrace("log.txt");
        pw.setTrace(trace);
        
        // creation of the two states "empty" and "not empty" with their invariant
        notEmpty = pw.createStatechart("Not Empty");
        notEmpty.stateInvariant(this,"stackNotEmpty");
        
        empty = pw.createStatechart("Empty");
        empty.stateInvariant(this, "stackEmpty");
        empty.inputState();
        empty.inputState();
        empty.inputState();
        empty.inputState();
        empty.inputState();
        
        // If you want to use the viewer, uncomment the two above lines and comment their next line
        //com.pauware.pauware_view.PauWare_view pv = new com.pauware.pauware_view.PauWare_view();
        stateMachine = pw.createStateMachine(empty.xor(notEmpty), "Stack SM", true, null);
        //stateMachine = new Statechart_monitor(empty.xor(notEmpty), "Stack SM", true);
        
        // Transition for the "push" event that will call the "actionPush". The String instances in
        // parameters are empty: they are juste here to precise the signature of the method
        //stateMachine.fires("push", empty, notEmpty, true, this, "actionPush", new Object[] {new String()});
        Transition t1 = new Transition("push", empty, notEmpty);
        t1.setBusinessObject(this);
        t1.setBusinessMethodName("actionPush");
        t1.setBusinessParams(new Object[] {new String()});
        stateMachine.setTransition(t1);
        
        //stateMachine.fires("push", notEmpty, notEmpty, true, this, "actionPush", new Object[] {new String()});
        Transition t2 = new Transition("push", notEmpty, notEmpty);
        t2.setBusinessObject(this);
        t2.setBusinessMethodName("actionPush");
        t2.setBusinessParams(new Object[] {new String()});
        stateMachine.setTransition(t2);
        
        
        // Transitions for the "pop" event with guards: if there more than one element in
        // the stack, the target is the "not empty" state, else, if there is only one 
        // element, the target is the "empty" state
        //stateMachine.fires("pop", notEmpty, empty, this, "onlyOne", this, "actionPop");
        Transition t3 = new Transition("pop", notEmpty, empty);
        t3.setGuardObject(this);
        t3.setGuardMethodName("onlyOne");
        t3.setBusinessObject(this);
        t3.setBusinessMethodName("actionPop");
        stateMachine.setTransition(t3);
        
        //stateMachine.fires("pop", notEmpty, notEmpty, this, "moreThanOne", this, "actionPop");
        Transition t4 = new Transition("pop", notEmpty, notEmpty);
        t4.setGuardObject(this);
        t4.setGuardMethodName("moreThanOne");
        t4.setBusinessObject(this);
        t4.setBusinessMethodName("actionPop");
        stateMachine.setTransition(t4);
        
        stateMachine.start();
    }
        
    public void stopStateMachine() throws Statechart_exception {
        stateMachine.stop();
    }
    
    public StackSMV2() {
        stack = new java.util.Stack();
    }
    
    
    public static void main(String argv[]) throws IOException  {

        try {
            StackSMV2 sm = new StackSMV2();
            sm.buildStateMachine();
            //sm.pushEvent("Foo");
            //sm.pushEvent("Bar");
            //sm.popEvent();
            //sm.popEvent();
            //sm.stopStateMachine();
            
            Scanner scan = new Scanner(System.in);
            String event = "";
            String value = "";
            
            // Events are entered by the user
            while (!event.equals("end")) {
                System.out.print("Enter an event name (\"end\" to finish): ");
                event = scan.nextLine();
                if (event.equals("push")) {
                    System.out.print("Enter a value : ");
                    value = scan.nextLine();
                    sm.pushEvent(value);
                }
                if (event.equals("pop")) {
                    sm.popEvent();
                }
                //if ()    
                //    sm.runEvent(event);
            }
            sm.stopStateMachine();
        }
        catch (Statechart_exception ex) {
            System.err.println("Error: "+ex);
        }
            
    }
}
